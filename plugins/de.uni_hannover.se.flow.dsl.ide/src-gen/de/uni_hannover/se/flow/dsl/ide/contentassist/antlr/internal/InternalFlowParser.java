package de.uni_hannover.se.flow.dsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import de.uni_hannover.se.flow.dsl.services.FlowGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFlowParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'in'", "'out'", "'supportedBy'", "'controlledBy'", "'fluid'", "'solid'", "'person'", "'domain'", "'task'", "'{'", "'}'", "'flows'", "'object'", "'by'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalFlowParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFlowParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFlowParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFlow.g"; }


    	private FlowGrammarAccess grammarAccess;

    	public void setGrammarAccess(FlowGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleInterview"
    // InternalFlow.g:53:1: entryRuleInterview : ruleInterview EOF ;
    public final void entryRuleInterview() throws RecognitionException {
        try {
            // InternalFlow.g:54:1: ( ruleInterview EOF )
            // InternalFlow.g:55:1: ruleInterview EOF
            {
             before(grammarAccess.getInterviewRule()); 
            pushFollow(FOLLOW_1);
            ruleInterview();

            state._fsp--;

             after(grammarAccess.getInterviewRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInterview"


    // $ANTLR start "ruleInterview"
    // InternalFlow.g:62:1: ruleInterview : ( ( rule__Interview__Group__0 ) ) ;
    public final void ruleInterview() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:66:2: ( ( ( rule__Interview__Group__0 ) ) )
            // InternalFlow.g:67:2: ( ( rule__Interview__Group__0 ) )
            {
            // InternalFlow.g:67:2: ( ( rule__Interview__Group__0 ) )
            // InternalFlow.g:68:3: ( rule__Interview__Group__0 )
            {
             before(grammarAccess.getInterviewAccess().getGroup()); 
            // InternalFlow.g:69:3: ( rule__Interview__Group__0 )
            // InternalFlow.g:69:4: rule__Interview__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Interview__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInterviewAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInterview"


    // $ANTLR start "entryRuleImport"
    // InternalFlow.g:78:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalFlow.g:79:1: ( ruleImport EOF )
            // InternalFlow.g:80:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalFlow.g:87:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:91:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalFlow.g:92:2: ( ( rule__Import__Group__0 ) )
            {
            // InternalFlow.g:92:2: ( ( rule__Import__Group__0 ) )
            // InternalFlow.g:93:3: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // InternalFlow.g:94:3: ( rule__Import__Group__0 )
            // InternalFlow.g:94:4: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleTask"
    // InternalFlow.g:103:1: entryRuleTask : ruleTask EOF ;
    public final void entryRuleTask() throws RecognitionException {
        try {
            // InternalFlow.g:104:1: ( ruleTask EOF )
            // InternalFlow.g:105:1: ruleTask EOF
            {
             before(grammarAccess.getTaskRule()); 
            pushFollow(FOLLOW_1);
            ruleTask();

            state._fsp--;

             after(grammarAccess.getTaskRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTask"


    // $ANTLR start "ruleTask"
    // InternalFlow.g:112:1: ruleTask : ( ( rule__Task__Group__0 ) ) ;
    public final void ruleTask() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:116:2: ( ( ( rule__Task__Group__0 ) ) )
            // InternalFlow.g:117:2: ( ( rule__Task__Group__0 ) )
            {
            // InternalFlow.g:117:2: ( ( rule__Task__Group__0 ) )
            // InternalFlow.g:118:3: ( rule__Task__Group__0 )
            {
             before(grammarAccess.getTaskAccess().getGroup()); 
            // InternalFlow.g:119:3: ( rule__Task__Group__0 )
            // InternalFlow.g:119:4: rule__Task__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Task__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTaskAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTask"


    // $ANTLR start "entryRuleFlows"
    // InternalFlow.g:128:1: entryRuleFlows : ruleFlows EOF ;
    public final void entryRuleFlows() throws RecognitionException {
        try {
            // InternalFlow.g:129:1: ( ruleFlows EOF )
            // InternalFlow.g:130:1: ruleFlows EOF
            {
             before(grammarAccess.getFlowsRule()); 
            pushFollow(FOLLOW_1);
            ruleFlows();

            state._fsp--;

             after(grammarAccess.getFlowsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFlows"


    // $ANTLR start "ruleFlows"
    // InternalFlow.g:137:1: ruleFlows : ( ( rule__Flows__Group__0 ) ) ;
    public final void ruleFlows() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:141:2: ( ( ( rule__Flows__Group__0 ) ) )
            // InternalFlow.g:142:2: ( ( rule__Flows__Group__0 ) )
            {
            // InternalFlow.g:142:2: ( ( rule__Flows__Group__0 ) )
            // InternalFlow.g:143:3: ( rule__Flows__Group__0 )
            {
             before(grammarAccess.getFlowsAccess().getGroup()); 
            // InternalFlow.g:144:3: ( rule__Flows__Group__0 )
            // InternalFlow.g:144:4: rule__Flows__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Flows__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFlowsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFlows"


    // $ANTLR start "entryRuleFlow"
    // InternalFlow.g:153:1: entryRuleFlow : ruleFlow EOF ;
    public final void entryRuleFlow() throws RecognitionException {
        try {
            // InternalFlow.g:154:1: ( ruleFlow EOF )
            // InternalFlow.g:155:1: ruleFlow EOF
            {
             before(grammarAccess.getFlowRule()); 
            pushFollow(FOLLOW_1);
            ruleFlow();

            state._fsp--;

             after(grammarAccess.getFlowRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFlow"


    // $ANTLR start "ruleFlow"
    // InternalFlow.g:162:1: ruleFlow : ( ( rule__Flow__Group__0 ) ) ;
    public final void ruleFlow() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:166:2: ( ( ( rule__Flow__Group__0 ) ) )
            // InternalFlow.g:167:2: ( ( rule__Flow__Group__0 ) )
            {
            // InternalFlow.g:167:2: ( ( rule__Flow__Group__0 ) )
            // InternalFlow.g:168:3: ( rule__Flow__Group__0 )
            {
             before(grammarAccess.getFlowAccess().getGroup()); 
            // InternalFlow.g:169:3: ( rule__Flow__Group__0 )
            // InternalFlow.g:169:4: rule__Flow__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Flow__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFlowAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFlow"


    // $ANTLR start "entryRuleKind"
    // InternalFlow.g:178:1: entryRuleKind : ruleKind EOF ;
    public final void entryRuleKind() throws RecognitionException {
        try {
            // InternalFlow.g:179:1: ( ruleKind EOF )
            // InternalFlow.g:180:1: ruleKind EOF
            {
             before(grammarAccess.getKindRule()); 
            pushFollow(FOLLOW_1);
            ruleKind();

            state._fsp--;

             after(grammarAccess.getKindRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleKind"


    // $ANTLR start "ruleKind"
    // InternalFlow.g:187:1: ruleKind : ( ( rule__Kind__Group__0 ) ) ;
    public final void ruleKind() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:191:2: ( ( ( rule__Kind__Group__0 ) ) )
            // InternalFlow.g:192:2: ( ( rule__Kind__Group__0 ) )
            {
            // InternalFlow.g:192:2: ( ( rule__Kind__Group__0 ) )
            // InternalFlow.g:193:3: ( rule__Kind__Group__0 )
            {
             before(grammarAccess.getKindAccess().getGroup()); 
            // InternalFlow.g:194:3: ( rule__Kind__Group__0 )
            // InternalFlow.g:194:4: rule__Kind__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Kind__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getKindAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleKind"


    // $ANTLR start "entryRuleMedium"
    // InternalFlow.g:203:1: entryRuleMedium : ruleMedium EOF ;
    public final void entryRuleMedium() throws RecognitionException {
        try {
            // InternalFlow.g:204:1: ( ruleMedium EOF )
            // InternalFlow.g:205:1: ruleMedium EOF
            {
             before(grammarAccess.getMediumRule()); 
            pushFollow(FOLLOW_1);
            ruleMedium();

            state._fsp--;

             after(grammarAccess.getMediumRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMedium"


    // $ANTLR start "ruleMedium"
    // InternalFlow.g:212:1: ruleMedium : ( ( rule__Medium__Group__0 ) ) ;
    public final void ruleMedium() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:216:2: ( ( ( rule__Medium__Group__0 ) ) )
            // InternalFlow.g:217:2: ( ( rule__Medium__Group__0 ) )
            {
            // InternalFlow.g:217:2: ( ( rule__Medium__Group__0 ) )
            // InternalFlow.g:218:3: ( rule__Medium__Group__0 )
            {
             before(grammarAccess.getMediumAccess().getGroup()); 
            // InternalFlow.g:219:3: ( rule__Medium__Group__0 )
            // InternalFlow.g:219:4: rule__Medium__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Medium__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMediumAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMedium"


    // $ANTLR start "ruleDirection"
    // InternalFlow.g:228:1: ruleDirection : ( ( rule__Direction__Alternatives ) ) ;
    public final void ruleDirection() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:232:1: ( ( ( rule__Direction__Alternatives ) ) )
            // InternalFlow.g:233:2: ( ( rule__Direction__Alternatives ) )
            {
            // InternalFlow.g:233:2: ( ( rule__Direction__Alternatives ) )
            // InternalFlow.g:234:3: ( rule__Direction__Alternatives )
            {
             before(grammarAccess.getDirectionAccess().getAlternatives()); 
            // InternalFlow.g:235:3: ( rule__Direction__Alternatives )
            // InternalFlow.g:235:4: rule__Direction__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Direction__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDirectionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDirection"


    // $ANTLR start "ruleType"
    // InternalFlow.g:244:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:248:1: ( ( ( rule__Type__Alternatives ) ) )
            // InternalFlow.g:249:2: ( ( rule__Type__Alternatives ) )
            {
            // InternalFlow.g:249:2: ( ( rule__Type__Alternatives ) )
            // InternalFlow.g:250:3: ( rule__Type__Alternatives )
            {
             before(grammarAccess.getTypeAccess().getAlternatives()); 
            // InternalFlow.g:251:3: ( rule__Type__Alternatives )
            // InternalFlow.g:251:4: rule__Type__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Type__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "rule__Kind__Alternatives_1"
    // InternalFlow.g:259:1: rule__Kind__Alternatives_1 : ( ( ( rule__Kind__Group_1_0__0 ) ) | ( ( rule__Kind__NameAssignment_1_1 ) ) );
    public final void rule__Kind__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:263:1: ( ( ( rule__Kind__Group_1_0__0 ) ) | ( ( rule__Kind__NameAssignment_1_1 ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==23) ) {
                alt1=1;
            }
            else if ( (LA1_0==RULE_STRING) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalFlow.g:264:2: ( ( rule__Kind__Group_1_0__0 ) )
                    {
                    // InternalFlow.g:264:2: ( ( rule__Kind__Group_1_0__0 ) )
                    // InternalFlow.g:265:3: ( rule__Kind__Group_1_0__0 )
                    {
                     before(grammarAccess.getKindAccess().getGroup_1_0()); 
                    // InternalFlow.g:266:3: ( rule__Kind__Group_1_0__0 )
                    // InternalFlow.g:266:4: rule__Kind__Group_1_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Kind__Group_1_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getKindAccess().getGroup_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFlow.g:270:2: ( ( rule__Kind__NameAssignment_1_1 ) )
                    {
                    // InternalFlow.g:270:2: ( ( rule__Kind__NameAssignment_1_1 ) )
                    // InternalFlow.g:271:3: ( rule__Kind__NameAssignment_1_1 )
                    {
                     before(grammarAccess.getKindAccess().getNameAssignment_1_1()); 
                    // InternalFlow.g:272:3: ( rule__Kind__NameAssignment_1_1 )
                    // InternalFlow.g:272:4: rule__Kind__NameAssignment_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Kind__NameAssignment_1_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getKindAccess().getNameAssignment_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__Alternatives_1"


    // $ANTLR start "rule__Direction__Alternatives"
    // InternalFlow.g:280:1: rule__Direction__Alternatives : ( ( ( 'in' ) ) | ( ( 'out' ) ) | ( ( 'supportedBy' ) ) | ( ( 'controlledBy' ) ) );
    public final void rule__Direction__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:284:1: ( ( ( 'in' ) ) | ( ( 'out' ) ) | ( ( 'supportedBy' ) ) | ( ( 'controlledBy' ) ) )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 12:
                {
                alt2=2;
                }
                break;
            case 13:
                {
                alt2=3;
                }
                break;
            case 14:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalFlow.g:285:2: ( ( 'in' ) )
                    {
                    // InternalFlow.g:285:2: ( ( 'in' ) )
                    // InternalFlow.g:286:3: ( 'in' )
                    {
                     before(grammarAccess.getDirectionAccess().getInEnumLiteralDeclaration_0()); 
                    // InternalFlow.g:287:3: ( 'in' )
                    // InternalFlow.g:287:4: 'in'
                    {
                    match(input,11,FOLLOW_2); 

                    }

                     after(grammarAccess.getDirectionAccess().getInEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFlow.g:291:2: ( ( 'out' ) )
                    {
                    // InternalFlow.g:291:2: ( ( 'out' ) )
                    // InternalFlow.g:292:3: ( 'out' )
                    {
                     before(grammarAccess.getDirectionAccess().getOutEnumLiteralDeclaration_1()); 
                    // InternalFlow.g:293:3: ( 'out' )
                    // InternalFlow.g:293:4: 'out'
                    {
                    match(input,12,FOLLOW_2); 

                    }

                     after(grammarAccess.getDirectionAccess().getOutEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalFlow.g:297:2: ( ( 'supportedBy' ) )
                    {
                    // InternalFlow.g:297:2: ( ( 'supportedBy' ) )
                    // InternalFlow.g:298:3: ( 'supportedBy' )
                    {
                     before(grammarAccess.getDirectionAccess().getSupportedByEnumLiteralDeclaration_2()); 
                    // InternalFlow.g:299:3: ( 'supportedBy' )
                    // InternalFlow.g:299:4: 'supportedBy'
                    {
                    match(input,13,FOLLOW_2); 

                    }

                     after(grammarAccess.getDirectionAccess().getSupportedByEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalFlow.g:303:2: ( ( 'controlledBy' ) )
                    {
                    // InternalFlow.g:303:2: ( ( 'controlledBy' ) )
                    // InternalFlow.g:304:3: ( 'controlledBy' )
                    {
                     before(grammarAccess.getDirectionAccess().getControlledByEnumLiteralDeclaration_3()); 
                    // InternalFlow.g:305:3: ( 'controlledBy' )
                    // InternalFlow.g:305:4: 'controlledBy'
                    {
                    match(input,14,FOLLOW_2); 

                    }

                     after(grammarAccess.getDirectionAccess().getControlledByEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Direction__Alternatives"


    // $ANTLR start "rule__Type__Alternatives"
    // InternalFlow.g:313:1: rule__Type__Alternatives : ( ( ( 'fluid' ) ) | ( ( 'solid' ) ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:317:1: ( ( ( 'fluid' ) ) | ( ( 'solid' ) ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==15) ) {
                alt3=1;
            }
            else if ( (LA3_0==16) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalFlow.g:318:2: ( ( 'fluid' ) )
                    {
                    // InternalFlow.g:318:2: ( ( 'fluid' ) )
                    // InternalFlow.g:319:3: ( 'fluid' )
                    {
                     before(grammarAccess.getTypeAccess().getFluidEnumLiteralDeclaration_0()); 
                    // InternalFlow.g:320:3: ( 'fluid' )
                    // InternalFlow.g:320:4: 'fluid'
                    {
                    match(input,15,FOLLOW_2); 

                    }

                     after(grammarAccess.getTypeAccess().getFluidEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFlow.g:324:2: ( ( 'solid' ) )
                    {
                    // InternalFlow.g:324:2: ( ( 'solid' ) )
                    // InternalFlow.g:325:3: ( 'solid' )
                    {
                     before(grammarAccess.getTypeAccess().getSolidEnumLiteralDeclaration_1()); 
                    // InternalFlow.g:326:3: ( 'solid' )
                    // InternalFlow.g:326:4: 'solid'
                    {
                    match(input,16,FOLLOW_2); 

                    }

                     after(grammarAccess.getTypeAccess().getSolidEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__Interview__Group__0"
    // InternalFlow.g:334:1: rule__Interview__Group__0 : rule__Interview__Group__0__Impl rule__Interview__Group__1 ;
    public final void rule__Interview__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:338:1: ( rule__Interview__Group__0__Impl rule__Interview__Group__1 )
            // InternalFlow.g:339:2: rule__Interview__Group__0__Impl rule__Interview__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Interview__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interview__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__Group__0"


    // $ANTLR start "rule__Interview__Group__0__Impl"
    // InternalFlow.g:346:1: rule__Interview__Group__0__Impl : ( ( rule__Interview__DomainModelAssignment_0 ) ) ;
    public final void rule__Interview__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:350:1: ( ( ( rule__Interview__DomainModelAssignment_0 ) ) )
            // InternalFlow.g:351:1: ( ( rule__Interview__DomainModelAssignment_0 ) )
            {
            // InternalFlow.g:351:1: ( ( rule__Interview__DomainModelAssignment_0 ) )
            // InternalFlow.g:352:2: ( rule__Interview__DomainModelAssignment_0 )
            {
             before(grammarAccess.getInterviewAccess().getDomainModelAssignment_0()); 
            // InternalFlow.g:353:2: ( rule__Interview__DomainModelAssignment_0 )
            // InternalFlow.g:353:3: rule__Interview__DomainModelAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Interview__DomainModelAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getInterviewAccess().getDomainModelAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__Group__0__Impl"


    // $ANTLR start "rule__Interview__Group__1"
    // InternalFlow.g:361:1: rule__Interview__Group__1 : rule__Interview__Group__1__Impl rule__Interview__Group__2 ;
    public final void rule__Interview__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:365:1: ( rule__Interview__Group__1__Impl rule__Interview__Group__2 )
            // InternalFlow.g:366:2: rule__Interview__Group__1__Impl rule__Interview__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Interview__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interview__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__Group__1"


    // $ANTLR start "rule__Interview__Group__1__Impl"
    // InternalFlow.g:373:1: rule__Interview__Group__1__Impl : ( 'person' ) ;
    public final void rule__Interview__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:377:1: ( ( 'person' ) )
            // InternalFlow.g:378:1: ( 'person' )
            {
            // InternalFlow.g:378:1: ( 'person' )
            // InternalFlow.g:379:2: 'person'
            {
             before(grammarAccess.getInterviewAccess().getPersonKeyword_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getInterviewAccess().getPersonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__Group__1__Impl"


    // $ANTLR start "rule__Interview__Group__2"
    // InternalFlow.g:388:1: rule__Interview__Group__2 : rule__Interview__Group__2__Impl rule__Interview__Group__3 ;
    public final void rule__Interview__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:392:1: ( rule__Interview__Group__2__Impl rule__Interview__Group__3 )
            // InternalFlow.g:393:2: rule__Interview__Group__2__Impl rule__Interview__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Interview__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interview__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__Group__2"


    // $ANTLR start "rule__Interview__Group__2__Impl"
    // InternalFlow.g:400:1: rule__Interview__Group__2__Impl : ( ( rule__Interview__PersonAssignment_2 ) ) ;
    public final void rule__Interview__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:404:1: ( ( ( rule__Interview__PersonAssignment_2 ) ) )
            // InternalFlow.g:405:1: ( ( rule__Interview__PersonAssignment_2 ) )
            {
            // InternalFlow.g:405:1: ( ( rule__Interview__PersonAssignment_2 ) )
            // InternalFlow.g:406:2: ( rule__Interview__PersonAssignment_2 )
            {
             before(grammarAccess.getInterviewAccess().getPersonAssignment_2()); 
            // InternalFlow.g:407:2: ( rule__Interview__PersonAssignment_2 )
            // InternalFlow.g:407:3: rule__Interview__PersonAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Interview__PersonAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getInterviewAccess().getPersonAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__Group__2__Impl"


    // $ANTLR start "rule__Interview__Group__3"
    // InternalFlow.g:415:1: rule__Interview__Group__3 : rule__Interview__Group__3__Impl rule__Interview__Group__4 ;
    public final void rule__Interview__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:419:1: ( rule__Interview__Group__3__Impl rule__Interview__Group__4 )
            // InternalFlow.g:420:2: rule__Interview__Group__3__Impl rule__Interview__Group__4
            {
            pushFollow(FOLLOW_5);
            rule__Interview__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interview__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__Group__3"


    // $ANTLR start "rule__Interview__Group__3__Impl"
    // InternalFlow.g:427:1: rule__Interview__Group__3__Impl : ( ( rule__Interview__TasksAssignment_3 )* ) ;
    public final void rule__Interview__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:431:1: ( ( ( rule__Interview__TasksAssignment_3 )* ) )
            // InternalFlow.g:432:1: ( ( rule__Interview__TasksAssignment_3 )* )
            {
            // InternalFlow.g:432:1: ( ( rule__Interview__TasksAssignment_3 )* )
            // InternalFlow.g:433:2: ( rule__Interview__TasksAssignment_3 )*
            {
             before(grammarAccess.getInterviewAccess().getTasksAssignment_3()); 
            // InternalFlow.g:434:2: ( rule__Interview__TasksAssignment_3 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==19) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalFlow.g:434:3: rule__Interview__TasksAssignment_3
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__Interview__TasksAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getInterviewAccess().getTasksAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__Group__3__Impl"


    // $ANTLR start "rule__Interview__Group__4"
    // InternalFlow.g:442:1: rule__Interview__Group__4 : rule__Interview__Group__4__Impl ;
    public final void rule__Interview__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:446:1: ( rule__Interview__Group__4__Impl )
            // InternalFlow.g:447:2: rule__Interview__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interview__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__Group__4"


    // $ANTLR start "rule__Interview__Group__4__Impl"
    // InternalFlow.g:453:1: rule__Interview__Group__4__Impl : ( ( rule__Interview__FlowsAssignment_4 )? ) ;
    public final void rule__Interview__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:457:1: ( ( ( rule__Interview__FlowsAssignment_4 )? ) )
            // InternalFlow.g:458:1: ( ( rule__Interview__FlowsAssignment_4 )? )
            {
            // InternalFlow.g:458:1: ( ( rule__Interview__FlowsAssignment_4 )? )
            // InternalFlow.g:459:2: ( rule__Interview__FlowsAssignment_4 )?
            {
             before(grammarAccess.getInterviewAccess().getFlowsAssignment_4()); 
            // InternalFlow.g:460:2: ( rule__Interview__FlowsAssignment_4 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==22) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalFlow.g:460:3: rule__Interview__FlowsAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Interview__FlowsAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getInterviewAccess().getFlowsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__Group__4__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalFlow.g:469:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:473:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalFlow.g:474:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalFlow.g:481:1: rule__Import__Group__0__Impl : ( 'domain' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:485:1: ( ( 'domain' ) )
            // InternalFlow.g:486:1: ( 'domain' )
            {
            // InternalFlow.g:486:1: ( 'domain' )
            // InternalFlow.g:487:2: 'domain'
            {
             before(grammarAccess.getImportAccess().getDomainKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getDomainKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalFlow.g:496:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:500:1: ( rule__Import__Group__1__Impl )
            // InternalFlow.g:501:2: rule__Import__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalFlow.g:507:1: rule__Import__Group__1__Impl : ( ( rule__Import__UriAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:511:1: ( ( ( rule__Import__UriAssignment_1 ) ) )
            // InternalFlow.g:512:1: ( ( rule__Import__UriAssignment_1 ) )
            {
            // InternalFlow.g:512:1: ( ( rule__Import__UriAssignment_1 ) )
            // InternalFlow.g:513:2: ( rule__Import__UriAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getUriAssignment_1()); 
            // InternalFlow.g:514:2: ( rule__Import__UriAssignment_1 )
            // InternalFlow.g:514:3: rule__Import__UriAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__UriAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getUriAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__Task__Group__0"
    // InternalFlow.g:523:1: rule__Task__Group__0 : rule__Task__Group__0__Impl rule__Task__Group__1 ;
    public final void rule__Task__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:527:1: ( rule__Task__Group__0__Impl rule__Task__Group__1 )
            // InternalFlow.g:528:2: rule__Task__Group__0__Impl rule__Task__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Task__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Task__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__Group__0"


    // $ANTLR start "rule__Task__Group__0__Impl"
    // InternalFlow.g:535:1: rule__Task__Group__0__Impl : ( 'task' ) ;
    public final void rule__Task__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:539:1: ( ( 'task' ) )
            // InternalFlow.g:540:1: ( 'task' )
            {
            // InternalFlow.g:540:1: ( 'task' )
            // InternalFlow.g:541:2: 'task'
            {
             before(grammarAccess.getTaskAccess().getTaskKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getTaskAccess().getTaskKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__Group__0__Impl"


    // $ANTLR start "rule__Task__Group__1"
    // InternalFlow.g:550:1: rule__Task__Group__1 : rule__Task__Group__1__Impl rule__Task__Group__2 ;
    public final void rule__Task__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:554:1: ( rule__Task__Group__1__Impl rule__Task__Group__2 )
            // InternalFlow.g:555:2: rule__Task__Group__1__Impl rule__Task__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Task__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Task__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__Group__1"


    // $ANTLR start "rule__Task__Group__1__Impl"
    // InternalFlow.g:562:1: rule__Task__Group__1__Impl : ( ( rule__Task__NameAssignment_1 ) ) ;
    public final void rule__Task__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:566:1: ( ( ( rule__Task__NameAssignment_1 ) ) )
            // InternalFlow.g:567:1: ( ( rule__Task__NameAssignment_1 ) )
            {
            // InternalFlow.g:567:1: ( ( rule__Task__NameAssignment_1 ) )
            // InternalFlow.g:568:2: ( rule__Task__NameAssignment_1 )
            {
             before(grammarAccess.getTaskAccess().getNameAssignment_1()); 
            // InternalFlow.g:569:2: ( rule__Task__NameAssignment_1 )
            // InternalFlow.g:569:3: rule__Task__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Task__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTaskAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__Group__1__Impl"


    // $ANTLR start "rule__Task__Group__2"
    // InternalFlow.g:577:1: rule__Task__Group__2 : rule__Task__Group__2__Impl rule__Task__Group__3 ;
    public final void rule__Task__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:581:1: ( rule__Task__Group__2__Impl rule__Task__Group__3 )
            // InternalFlow.g:582:2: rule__Task__Group__2__Impl rule__Task__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Task__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Task__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__Group__2"


    // $ANTLR start "rule__Task__Group__2__Impl"
    // InternalFlow.g:589:1: rule__Task__Group__2__Impl : ( '{' ) ;
    public final void rule__Task__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:593:1: ( ( '{' ) )
            // InternalFlow.g:594:1: ( '{' )
            {
            // InternalFlow.g:594:1: ( '{' )
            // InternalFlow.g:595:2: '{'
            {
             before(grammarAccess.getTaskAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getTaskAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__Group__2__Impl"


    // $ANTLR start "rule__Task__Group__3"
    // InternalFlow.g:604:1: rule__Task__Group__3 : rule__Task__Group__3__Impl rule__Task__Group__4 ;
    public final void rule__Task__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:608:1: ( rule__Task__Group__3__Impl rule__Task__Group__4 )
            // InternalFlow.g:609:2: rule__Task__Group__3__Impl rule__Task__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__Task__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Task__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__Group__3"


    // $ANTLR start "rule__Task__Group__3__Impl"
    // InternalFlow.g:616:1: rule__Task__Group__3__Impl : ( ( rule__Task__FlowsAssignment_3 )* ) ;
    public final void rule__Task__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:620:1: ( ( ( rule__Task__FlowsAssignment_3 )* ) )
            // InternalFlow.g:621:1: ( ( rule__Task__FlowsAssignment_3 )* )
            {
            // InternalFlow.g:621:1: ( ( rule__Task__FlowsAssignment_3 )* )
            // InternalFlow.g:622:2: ( rule__Task__FlowsAssignment_3 )*
            {
             before(grammarAccess.getTaskAccess().getFlowsAssignment_3()); 
            // InternalFlow.g:623:2: ( rule__Task__FlowsAssignment_3 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>=11 && LA6_0<=14)) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalFlow.g:623:3: rule__Task__FlowsAssignment_3
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Task__FlowsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getTaskAccess().getFlowsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__Group__3__Impl"


    // $ANTLR start "rule__Task__Group__4"
    // InternalFlow.g:631:1: rule__Task__Group__4 : rule__Task__Group__4__Impl ;
    public final void rule__Task__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:635:1: ( rule__Task__Group__4__Impl )
            // InternalFlow.g:636:2: rule__Task__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Task__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__Group__4"


    // $ANTLR start "rule__Task__Group__4__Impl"
    // InternalFlow.g:642:1: rule__Task__Group__4__Impl : ( '}' ) ;
    public final void rule__Task__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:646:1: ( ( '}' ) )
            // InternalFlow.g:647:1: ( '}' )
            {
            // InternalFlow.g:647:1: ( '}' )
            // InternalFlow.g:648:2: '}'
            {
             before(grammarAccess.getTaskAccess().getRightCurlyBracketKeyword_4()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getTaskAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__Group__4__Impl"


    // $ANTLR start "rule__Flows__Group__0"
    // InternalFlow.g:658:1: rule__Flows__Group__0 : rule__Flows__Group__0__Impl rule__Flows__Group__1 ;
    public final void rule__Flows__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:662:1: ( rule__Flows__Group__0__Impl rule__Flows__Group__1 )
            // InternalFlow.g:663:2: rule__Flows__Group__0__Impl rule__Flows__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Flows__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Flows__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flows__Group__0"


    // $ANTLR start "rule__Flows__Group__0__Impl"
    // InternalFlow.g:670:1: rule__Flows__Group__0__Impl : ( () ) ;
    public final void rule__Flows__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:674:1: ( ( () ) )
            // InternalFlow.g:675:1: ( () )
            {
            // InternalFlow.g:675:1: ( () )
            // InternalFlow.g:676:2: ()
            {
             before(grammarAccess.getFlowsAccess().getFlowsAction_0()); 
            // InternalFlow.g:677:2: ()
            // InternalFlow.g:677:3: 
            {
            }

             after(grammarAccess.getFlowsAccess().getFlowsAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flows__Group__0__Impl"


    // $ANTLR start "rule__Flows__Group__1"
    // InternalFlow.g:685:1: rule__Flows__Group__1 : rule__Flows__Group__1__Impl rule__Flows__Group__2 ;
    public final void rule__Flows__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:689:1: ( rule__Flows__Group__1__Impl rule__Flows__Group__2 )
            // InternalFlow.g:690:2: rule__Flows__Group__1__Impl rule__Flows__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Flows__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Flows__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flows__Group__1"


    // $ANTLR start "rule__Flows__Group__1__Impl"
    // InternalFlow.g:697:1: rule__Flows__Group__1__Impl : ( 'flows' ) ;
    public final void rule__Flows__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:701:1: ( ( 'flows' ) )
            // InternalFlow.g:702:1: ( 'flows' )
            {
            // InternalFlow.g:702:1: ( 'flows' )
            // InternalFlow.g:703:2: 'flows'
            {
             before(grammarAccess.getFlowsAccess().getFlowsKeyword_1()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getFlowsAccess().getFlowsKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flows__Group__1__Impl"


    // $ANTLR start "rule__Flows__Group__2"
    // InternalFlow.g:712:1: rule__Flows__Group__2 : rule__Flows__Group__2__Impl rule__Flows__Group__3 ;
    public final void rule__Flows__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:716:1: ( rule__Flows__Group__2__Impl rule__Flows__Group__3 )
            // InternalFlow.g:717:2: rule__Flows__Group__2__Impl rule__Flows__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Flows__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Flows__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flows__Group__2"


    // $ANTLR start "rule__Flows__Group__2__Impl"
    // InternalFlow.g:724:1: rule__Flows__Group__2__Impl : ( '{' ) ;
    public final void rule__Flows__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:728:1: ( ( '{' ) )
            // InternalFlow.g:729:1: ( '{' )
            {
            // InternalFlow.g:729:1: ( '{' )
            // InternalFlow.g:730:2: '{'
            {
             before(grammarAccess.getFlowsAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getFlowsAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flows__Group__2__Impl"


    // $ANTLR start "rule__Flows__Group__3"
    // InternalFlow.g:739:1: rule__Flows__Group__3 : rule__Flows__Group__3__Impl rule__Flows__Group__4 ;
    public final void rule__Flows__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:743:1: ( rule__Flows__Group__3__Impl rule__Flows__Group__4 )
            // InternalFlow.g:744:2: rule__Flows__Group__3__Impl rule__Flows__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__Flows__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Flows__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flows__Group__3"


    // $ANTLR start "rule__Flows__Group__3__Impl"
    // InternalFlow.g:751:1: rule__Flows__Group__3__Impl : ( ( rule__Flows__FlowsAssignment_3 )* ) ;
    public final void rule__Flows__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:755:1: ( ( ( rule__Flows__FlowsAssignment_3 )* ) )
            // InternalFlow.g:756:1: ( ( rule__Flows__FlowsAssignment_3 )* )
            {
            // InternalFlow.g:756:1: ( ( rule__Flows__FlowsAssignment_3 )* )
            // InternalFlow.g:757:2: ( rule__Flows__FlowsAssignment_3 )*
            {
             before(grammarAccess.getFlowsAccess().getFlowsAssignment_3()); 
            // InternalFlow.g:758:2: ( rule__Flows__FlowsAssignment_3 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>=11 && LA7_0<=14)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalFlow.g:758:3: rule__Flows__FlowsAssignment_3
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Flows__FlowsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getFlowsAccess().getFlowsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flows__Group__3__Impl"


    // $ANTLR start "rule__Flows__Group__4"
    // InternalFlow.g:766:1: rule__Flows__Group__4 : rule__Flows__Group__4__Impl ;
    public final void rule__Flows__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:770:1: ( rule__Flows__Group__4__Impl )
            // InternalFlow.g:771:2: rule__Flows__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Flows__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flows__Group__4"


    // $ANTLR start "rule__Flows__Group__4__Impl"
    // InternalFlow.g:777:1: rule__Flows__Group__4__Impl : ( '}' ) ;
    public final void rule__Flows__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:781:1: ( ( '}' ) )
            // InternalFlow.g:782:1: ( '}' )
            {
            // InternalFlow.g:782:1: ( '}' )
            // InternalFlow.g:783:2: '}'
            {
             before(grammarAccess.getFlowsAccess().getRightCurlyBracketKeyword_4()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getFlowsAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flows__Group__4__Impl"


    // $ANTLR start "rule__Flow__Group__0"
    // InternalFlow.g:793:1: rule__Flow__Group__0 : rule__Flow__Group__0__Impl rule__Flow__Group__1 ;
    public final void rule__Flow__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:797:1: ( rule__Flow__Group__0__Impl rule__Flow__Group__1 )
            // InternalFlow.g:798:2: rule__Flow__Group__0__Impl rule__Flow__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Flow__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Flow__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__Group__0"


    // $ANTLR start "rule__Flow__Group__0__Impl"
    // InternalFlow.g:805:1: rule__Flow__Group__0__Impl : ( ( rule__Flow__DirectionAssignment_0 ) ) ;
    public final void rule__Flow__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:809:1: ( ( ( rule__Flow__DirectionAssignment_0 ) ) )
            // InternalFlow.g:810:1: ( ( rule__Flow__DirectionAssignment_0 ) )
            {
            // InternalFlow.g:810:1: ( ( rule__Flow__DirectionAssignment_0 ) )
            // InternalFlow.g:811:2: ( rule__Flow__DirectionAssignment_0 )
            {
             before(grammarAccess.getFlowAccess().getDirectionAssignment_0()); 
            // InternalFlow.g:812:2: ( rule__Flow__DirectionAssignment_0 )
            // InternalFlow.g:812:3: rule__Flow__DirectionAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Flow__DirectionAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFlowAccess().getDirectionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__Group__0__Impl"


    // $ANTLR start "rule__Flow__Group__1"
    // InternalFlow.g:820:1: rule__Flow__Group__1 : rule__Flow__Group__1__Impl rule__Flow__Group__2 ;
    public final void rule__Flow__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:824:1: ( rule__Flow__Group__1__Impl rule__Flow__Group__2 )
            // InternalFlow.g:825:2: rule__Flow__Group__1__Impl rule__Flow__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__Flow__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Flow__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__Group__1"


    // $ANTLR start "rule__Flow__Group__1__Impl"
    // InternalFlow.g:832:1: rule__Flow__Group__1__Impl : ( ( rule__Flow__OtherAssignment_1 ) ) ;
    public final void rule__Flow__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:836:1: ( ( ( rule__Flow__OtherAssignment_1 ) ) )
            // InternalFlow.g:837:1: ( ( rule__Flow__OtherAssignment_1 ) )
            {
            // InternalFlow.g:837:1: ( ( rule__Flow__OtherAssignment_1 ) )
            // InternalFlow.g:838:2: ( rule__Flow__OtherAssignment_1 )
            {
             before(grammarAccess.getFlowAccess().getOtherAssignment_1()); 
            // InternalFlow.g:839:2: ( rule__Flow__OtherAssignment_1 )
            // InternalFlow.g:839:3: rule__Flow__OtherAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Flow__OtherAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFlowAccess().getOtherAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__Group__1__Impl"


    // $ANTLR start "rule__Flow__Group__2"
    // InternalFlow.g:847:1: rule__Flow__Group__2 : rule__Flow__Group__2__Impl rule__Flow__Group__3 ;
    public final void rule__Flow__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:851:1: ( rule__Flow__Group__2__Impl rule__Flow__Group__3 )
            // InternalFlow.g:852:2: rule__Flow__Group__2__Impl rule__Flow__Group__3
            {
            pushFollow(FOLLOW_13);
            rule__Flow__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Flow__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__Group__2"


    // $ANTLR start "rule__Flow__Group__2__Impl"
    // InternalFlow.g:859:1: rule__Flow__Group__2__Impl : ( ( rule__Flow__TypeAssignment_2 ) ) ;
    public final void rule__Flow__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:863:1: ( ( ( rule__Flow__TypeAssignment_2 ) ) )
            // InternalFlow.g:864:1: ( ( rule__Flow__TypeAssignment_2 ) )
            {
            // InternalFlow.g:864:1: ( ( rule__Flow__TypeAssignment_2 ) )
            // InternalFlow.g:865:2: ( rule__Flow__TypeAssignment_2 )
            {
             before(grammarAccess.getFlowAccess().getTypeAssignment_2()); 
            // InternalFlow.g:866:2: ( rule__Flow__TypeAssignment_2 )
            // InternalFlow.g:866:3: rule__Flow__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Flow__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFlowAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__Group__2__Impl"


    // $ANTLR start "rule__Flow__Group__3"
    // InternalFlow.g:874:1: rule__Flow__Group__3 : rule__Flow__Group__3__Impl rule__Flow__Group__4 ;
    public final void rule__Flow__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:878:1: ( rule__Flow__Group__3__Impl rule__Flow__Group__4 )
            // InternalFlow.g:879:2: rule__Flow__Group__3__Impl rule__Flow__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__Flow__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Flow__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__Group__3"


    // $ANTLR start "rule__Flow__Group__3__Impl"
    // InternalFlow.g:886:1: rule__Flow__Group__3__Impl : ( ( rule__Flow__KindAssignment_3 )? ) ;
    public final void rule__Flow__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:890:1: ( ( ( rule__Flow__KindAssignment_3 )? ) )
            // InternalFlow.g:891:1: ( ( rule__Flow__KindAssignment_3 )? )
            {
            // InternalFlow.g:891:1: ( ( rule__Flow__KindAssignment_3 )? )
            // InternalFlow.g:892:2: ( rule__Flow__KindAssignment_3 )?
            {
             before(grammarAccess.getFlowAccess().getKindAssignment_3()); 
            // InternalFlow.g:893:2: ( rule__Flow__KindAssignment_3 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_STRING||LA8_0==23) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalFlow.g:893:3: rule__Flow__KindAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Flow__KindAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFlowAccess().getKindAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__Group__3__Impl"


    // $ANTLR start "rule__Flow__Group__4"
    // InternalFlow.g:901:1: rule__Flow__Group__4 : rule__Flow__Group__4__Impl ;
    public final void rule__Flow__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:905:1: ( rule__Flow__Group__4__Impl )
            // InternalFlow.g:906:2: rule__Flow__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Flow__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__Group__4"


    // $ANTLR start "rule__Flow__Group__4__Impl"
    // InternalFlow.g:912:1: rule__Flow__Group__4__Impl : ( ( rule__Flow__MediumAssignment_4 )? ) ;
    public final void rule__Flow__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:916:1: ( ( ( rule__Flow__MediumAssignment_4 )? ) )
            // InternalFlow.g:917:1: ( ( rule__Flow__MediumAssignment_4 )? )
            {
            // InternalFlow.g:917:1: ( ( rule__Flow__MediumAssignment_4 )? )
            // InternalFlow.g:918:2: ( rule__Flow__MediumAssignment_4 )?
            {
             before(grammarAccess.getFlowAccess().getMediumAssignment_4()); 
            // InternalFlow.g:919:2: ( rule__Flow__MediumAssignment_4 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==24) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalFlow.g:919:3: rule__Flow__MediumAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Flow__MediumAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFlowAccess().getMediumAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__Group__4__Impl"


    // $ANTLR start "rule__Kind__Group__0"
    // InternalFlow.g:928:1: rule__Kind__Group__0 : rule__Kind__Group__0__Impl rule__Kind__Group__1 ;
    public final void rule__Kind__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:932:1: ( rule__Kind__Group__0__Impl rule__Kind__Group__1 )
            // InternalFlow.g:933:2: rule__Kind__Group__0__Impl rule__Kind__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Kind__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Kind__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__Group__0"


    // $ANTLR start "rule__Kind__Group__0__Impl"
    // InternalFlow.g:940:1: rule__Kind__Group__0__Impl : ( () ) ;
    public final void rule__Kind__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:944:1: ( ( () ) )
            // InternalFlow.g:945:1: ( () )
            {
            // InternalFlow.g:945:1: ( () )
            // InternalFlow.g:946:2: ()
            {
             before(grammarAccess.getKindAccess().getKindAction_0()); 
            // InternalFlow.g:947:2: ()
            // InternalFlow.g:947:3: 
            {
            }

             after(grammarAccess.getKindAccess().getKindAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__Group__0__Impl"


    // $ANTLR start "rule__Kind__Group__1"
    // InternalFlow.g:955:1: rule__Kind__Group__1 : rule__Kind__Group__1__Impl ;
    public final void rule__Kind__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:959:1: ( rule__Kind__Group__1__Impl )
            // InternalFlow.g:960:2: rule__Kind__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Kind__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__Group__1"


    // $ANTLR start "rule__Kind__Group__1__Impl"
    // InternalFlow.g:966:1: rule__Kind__Group__1__Impl : ( ( rule__Kind__Alternatives_1 ) ) ;
    public final void rule__Kind__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:970:1: ( ( ( rule__Kind__Alternatives_1 ) ) )
            // InternalFlow.g:971:1: ( ( rule__Kind__Alternatives_1 ) )
            {
            // InternalFlow.g:971:1: ( ( rule__Kind__Alternatives_1 ) )
            // InternalFlow.g:972:2: ( rule__Kind__Alternatives_1 )
            {
             before(grammarAccess.getKindAccess().getAlternatives_1()); 
            // InternalFlow.g:973:2: ( rule__Kind__Alternatives_1 )
            // InternalFlow.g:973:3: rule__Kind__Alternatives_1
            {
            pushFollow(FOLLOW_2);
            rule__Kind__Alternatives_1();

            state._fsp--;


            }

             after(grammarAccess.getKindAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__Group__1__Impl"


    // $ANTLR start "rule__Kind__Group_1_0__0"
    // InternalFlow.g:982:1: rule__Kind__Group_1_0__0 : rule__Kind__Group_1_0__0__Impl rule__Kind__Group_1_0__1 ;
    public final void rule__Kind__Group_1_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:986:1: ( rule__Kind__Group_1_0__0__Impl rule__Kind__Group_1_0__1 )
            // InternalFlow.g:987:2: rule__Kind__Group_1_0__0__Impl rule__Kind__Group_1_0__1
            {
            pushFollow(FOLLOW_4);
            rule__Kind__Group_1_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Kind__Group_1_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__Group_1_0__0"


    // $ANTLR start "rule__Kind__Group_1_0__0__Impl"
    // InternalFlow.g:994:1: rule__Kind__Group_1_0__0__Impl : ( 'object' ) ;
    public final void rule__Kind__Group_1_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:998:1: ( ( 'object' ) )
            // InternalFlow.g:999:1: ( 'object' )
            {
            // InternalFlow.g:999:1: ( 'object' )
            // InternalFlow.g:1000:2: 'object'
            {
             before(grammarAccess.getKindAccess().getObjectKeyword_1_0_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getKindAccess().getObjectKeyword_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__Group_1_0__0__Impl"


    // $ANTLR start "rule__Kind__Group_1_0__1"
    // InternalFlow.g:1009:1: rule__Kind__Group_1_0__1 : rule__Kind__Group_1_0__1__Impl ;
    public final void rule__Kind__Group_1_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1013:1: ( rule__Kind__Group_1_0__1__Impl )
            // InternalFlow.g:1014:2: rule__Kind__Group_1_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Kind__Group_1_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__Group_1_0__1"


    // $ANTLR start "rule__Kind__Group_1_0__1__Impl"
    // InternalFlow.g:1020:1: rule__Kind__Group_1_0__1__Impl : ( ( rule__Kind__ObjectAssignment_1_0_1 ) ) ;
    public final void rule__Kind__Group_1_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1024:1: ( ( ( rule__Kind__ObjectAssignment_1_0_1 ) ) )
            // InternalFlow.g:1025:1: ( ( rule__Kind__ObjectAssignment_1_0_1 ) )
            {
            // InternalFlow.g:1025:1: ( ( rule__Kind__ObjectAssignment_1_0_1 ) )
            // InternalFlow.g:1026:2: ( rule__Kind__ObjectAssignment_1_0_1 )
            {
             before(grammarAccess.getKindAccess().getObjectAssignment_1_0_1()); 
            // InternalFlow.g:1027:2: ( rule__Kind__ObjectAssignment_1_0_1 )
            // InternalFlow.g:1027:3: rule__Kind__ObjectAssignment_1_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Kind__ObjectAssignment_1_0_1();

            state._fsp--;


            }

             after(grammarAccess.getKindAccess().getObjectAssignment_1_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__Group_1_0__1__Impl"


    // $ANTLR start "rule__Medium__Group__0"
    // InternalFlow.g:1036:1: rule__Medium__Group__0 : rule__Medium__Group__0__Impl rule__Medium__Group__1 ;
    public final void rule__Medium__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1040:1: ( rule__Medium__Group__0__Impl rule__Medium__Group__1 )
            // InternalFlow.g:1041:2: rule__Medium__Group__0__Impl rule__Medium__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Medium__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Medium__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Medium__Group__0"


    // $ANTLR start "rule__Medium__Group__0__Impl"
    // InternalFlow.g:1048:1: rule__Medium__Group__0__Impl : ( 'by' ) ;
    public final void rule__Medium__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1052:1: ( ( 'by' ) )
            // InternalFlow.g:1053:1: ( 'by' )
            {
            // InternalFlow.g:1053:1: ( 'by' )
            // InternalFlow.g:1054:2: 'by'
            {
             before(grammarAccess.getMediumAccess().getByKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getMediumAccess().getByKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Medium__Group__0__Impl"


    // $ANTLR start "rule__Medium__Group__1"
    // InternalFlow.g:1063:1: rule__Medium__Group__1 : rule__Medium__Group__1__Impl ;
    public final void rule__Medium__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1067:1: ( rule__Medium__Group__1__Impl )
            // InternalFlow.g:1068:2: rule__Medium__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Medium__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Medium__Group__1"


    // $ANTLR start "rule__Medium__Group__1__Impl"
    // InternalFlow.g:1074:1: rule__Medium__Group__1__Impl : ( ( rule__Medium__NameAssignment_1 ) ) ;
    public final void rule__Medium__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1078:1: ( ( ( rule__Medium__NameAssignment_1 ) ) )
            // InternalFlow.g:1079:1: ( ( rule__Medium__NameAssignment_1 ) )
            {
            // InternalFlow.g:1079:1: ( ( rule__Medium__NameAssignment_1 ) )
            // InternalFlow.g:1080:2: ( rule__Medium__NameAssignment_1 )
            {
             before(grammarAccess.getMediumAccess().getNameAssignment_1()); 
            // InternalFlow.g:1081:2: ( rule__Medium__NameAssignment_1 )
            // InternalFlow.g:1081:3: rule__Medium__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Medium__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMediumAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Medium__Group__1__Impl"


    // $ANTLR start "rule__Interview__DomainModelAssignment_0"
    // InternalFlow.g:1090:1: rule__Interview__DomainModelAssignment_0 : ( ruleImport ) ;
    public final void rule__Interview__DomainModelAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1094:1: ( ( ruleImport ) )
            // InternalFlow.g:1095:2: ( ruleImport )
            {
            // InternalFlow.g:1095:2: ( ruleImport )
            // InternalFlow.g:1096:3: ruleImport
            {
             before(grammarAccess.getInterviewAccess().getDomainModelImportParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getInterviewAccess().getDomainModelImportParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__DomainModelAssignment_0"


    // $ANTLR start "rule__Interview__PersonAssignment_2"
    // InternalFlow.g:1105:1: rule__Interview__PersonAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__Interview__PersonAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1109:1: ( ( ( RULE_ID ) ) )
            // InternalFlow.g:1110:2: ( ( RULE_ID ) )
            {
            // InternalFlow.g:1110:2: ( ( RULE_ID ) )
            // InternalFlow.g:1111:3: ( RULE_ID )
            {
             before(grammarAccess.getInterviewAccess().getPersonEClassCrossReference_2_0()); 
            // InternalFlow.g:1112:3: ( RULE_ID )
            // InternalFlow.g:1113:4: RULE_ID
            {
             before(grammarAccess.getInterviewAccess().getPersonEClassIDTerminalRuleCall_2_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getInterviewAccess().getPersonEClassIDTerminalRuleCall_2_0_1()); 

            }

             after(grammarAccess.getInterviewAccess().getPersonEClassCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__PersonAssignment_2"


    // $ANTLR start "rule__Interview__TasksAssignment_3"
    // InternalFlow.g:1124:1: rule__Interview__TasksAssignment_3 : ( ruleTask ) ;
    public final void rule__Interview__TasksAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1128:1: ( ( ruleTask ) )
            // InternalFlow.g:1129:2: ( ruleTask )
            {
            // InternalFlow.g:1129:2: ( ruleTask )
            // InternalFlow.g:1130:3: ruleTask
            {
             before(grammarAccess.getInterviewAccess().getTasksTaskParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleTask();

            state._fsp--;

             after(grammarAccess.getInterviewAccess().getTasksTaskParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__TasksAssignment_3"


    // $ANTLR start "rule__Interview__FlowsAssignment_4"
    // InternalFlow.g:1139:1: rule__Interview__FlowsAssignment_4 : ( ruleFlows ) ;
    public final void rule__Interview__FlowsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1143:1: ( ( ruleFlows ) )
            // InternalFlow.g:1144:2: ( ruleFlows )
            {
            // InternalFlow.g:1144:2: ( ruleFlows )
            // InternalFlow.g:1145:3: ruleFlows
            {
             before(grammarAccess.getInterviewAccess().getFlowsFlowsParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleFlows();

            state._fsp--;

             after(grammarAccess.getInterviewAccess().getFlowsFlowsParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interview__FlowsAssignment_4"


    // $ANTLR start "rule__Import__UriAssignment_1"
    // InternalFlow.g:1154:1: rule__Import__UriAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Import__UriAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1158:1: ( ( RULE_STRING ) )
            // InternalFlow.g:1159:2: ( RULE_STRING )
            {
            // InternalFlow.g:1159:2: ( RULE_STRING )
            // InternalFlow.g:1160:3: RULE_STRING
            {
             before(grammarAccess.getImportAccess().getUriSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getUriSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__UriAssignment_1"


    // $ANTLR start "rule__Task__NameAssignment_1"
    // InternalFlow.g:1169:1: rule__Task__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Task__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1173:1: ( ( RULE_ID ) )
            // InternalFlow.g:1174:2: ( RULE_ID )
            {
            // InternalFlow.g:1174:2: ( RULE_ID )
            // InternalFlow.g:1175:3: RULE_ID
            {
             before(grammarAccess.getTaskAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTaskAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__NameAssignment_1"


    // $ANTLR start "rule__Task__FlowsAssignment_3"
    // InternalFlow.g:1184:1: rule__Task__FlowsAssignment_3 : ( ruleFlow ) ;
    public final void rule__Task__FlowsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1188:1: ( ( ruleFlow ) )
            // InternalFlow.g:1189:2: ( ruleFlow )
            {
            // InternalFlow.g:1189:2: ( ruleFlow )
            // InternalFlow.g:1190:3: ruleFlow
            {
             before(grammarAccess.getTaskAccess().getFlowsFlowParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleFlow();

            state._fsp--;

             after(grammarAccess.getTaskAccess().getFlowsFlowParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Task__FlowsAssignment_3"


    // $ANTLR start "rule__Flows__FlowsAssignment_3"
    // InternalFlow.g:1199:1: rule__Flows__FlowsAssignment_3 : ( ruleFlow ) ;
    public final void rule__Flows__FlowsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1203:1: ( ( ruleFlow ) )
            // InternalFlow.g:1204:2: ( ruleFlow )
            {
            // InternalFlow.g:1204:2: ( ruleFlow )
            // InternalFlow.g:1205:3: ruleFlow
            {
             before(grammarAccess.getFlowsAccess().getFlowsFlowParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleFlow();

            state._fsp--;

             after(grammarAccess.getFlowsAccess().getFlowsFlowParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flows__FlowsAssignment_3"


    // $ANTLR start "rule__Flow__DirectionAssignment_0"
    // InternalFlow.g:1214:1: rule__Flow__DirectionAssignment_0 : ( ruleDirection ) ;
    public final void rule__Flow__DirectionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1218:1: ( ( ruleDirection ) )
            // InternalFlow.g:1219:2: ( ruleDirection )
            {
            // InternalFlow.g:1219:2: ( ruleDirection )
            // InternalFlow.g:1220:3: ruleDirection
            {
             before(grammarAccess.getFlowAccess().getDirectionDirectionEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDirection();

            state._fsp--;

             after(grammarAccess.getFlowAccess().getDirectionDirectionEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__DirectionAssignment_0"


    // $ANTLR start "rule__Flow__OtherAssignment_1"
    // InternalFlow.g:1229:1: rule__Flow__OtherAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__Flow__OtherAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1233:1: ( ( ( RULE_ID ) ) )
            // InternalFlow.g:1234:2: ( ( RULE_ID ) )
            {
            // InternalFlow.g:1234:2: ( ( RULE_ID ) )
            // InternalFlow.g:1235:3: ( RULE_ID )
            {
             before(grammarAccess.getFlowAccess().getOtherEClassCrossReference_1_0()); 
            // InternalFlow.g:1236:3: ( RULE_ID )
            // InternalFlow.g:1237:4: RULE_ID
            {
             before(grammarAccess.getFlowAccess().getOtherEClassIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFlowAccess().getOtherEClassIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getFlowAccess().getOtherEClassCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__OtherAssignment_1"


    // $ANTLR start "rule__Flow__TypeAssignment_2"
    // InternalFlow.g:1248:1: rule__Flow__TypeAssignment_2 : ( ruleType ) ;
    public final void rule__Flow__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1252:1: ( ( ruleType ) )
            // InternalFlow.g:1253:2: ( ruleType )
            {
            // InternalFlow.g:1253:2: ( ruleType )
            // InternalFlow.g:1254:3: ruleType
            {
             before(grammarAccess.getFlowAccess().getTypeTypeEnumRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getFlowAccess().getTypeTypeEnumRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__TypeAssignment_2"


    // $ANTLR start "rule__Flow__KindAssignment_3"
    // InternalFlow.g:1263:1: rule__Flow__KindAssignment_3 : ( ruleKind ) ;
    public final void rule__Flow__KindAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1267:1: ( ( ruleKind ) )
            // InternalFlow.g:1268:2: ( ruleKind )
            {
            // InternalFlow.g:1268:2: ( ruleKind )
            // InternalFlow.g:1269:3: ruleKind
            {
             before(grammarAccess.getFlowAccess().getKindKindParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleKind();

            state._fsp--;

             after(grammarAccess.getFlowAccess().getKindKindParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__KindAssignment_3"


    // $ANTLR start "rule__Flow__MediumAssignment_4"
    // InternalFlow.g:1278:1: rule__Flow__MediumAssignment_4 : ( ruleMedium ) ;
    public final void rule__Flow__MediumAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1282:1: ( ( ruleMedium ) )
            // InternalFlow.g:1283:2: ( ruleMedium )
            {
            // InternalFlow.g:1283:2: ( ruleMedium )
            // InternalFlow.g:1284:3: ruleMedium
            {
             before(grammarAccess.getFlowAccess().getMediumMediumParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleMedium();

            state._fsp--;

             after(grammarAccess.getFlowAccess().getMediumMediumParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Flow__MediumAssignment_4"


    // $ANTLR start "rule__Kind__ObjectAssignment_1_0_1"
    // InternalFlow.g:1293:1: rule__Kind__ObjectAssignment_1_0_1 : ( ( RULE_ID ) ) ;
    public final void rule__Kind__ObjectAssignment_1_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1297:1: ( ( ( RULE_ID ) ) )
            // InternalFlow.g:1298:2: ( ( RULE_ID ) )
            {
            // InternalFlow.g:1298:2: ( ( RULE_ID ) )
            // InternalFlow.g:1299:3: ( RULE_ID )
            {
             before(grammarAccess.getKindAccess().getObjectEClassCrossReference_1_0_1_0()); 
            // InternalFlow.g:1300:3: ( RULE_ID )
            // InternalFlow.g:1301:4: RULE_ID
            {
             before(grammarAccess.getKindAccess().getObjectEClassIDTerminalRuleCall_1_0_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getKindAccess().getObjectEClassIDTerminalRuleCall_1_0_1_0_1()); 

            }

             after(grammarAccess.getKindAccess().getObjectEClassCrossReference_1_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__ObjectAssignment_1_0_1"


    // $ANTLR start "rule__Kind__NameAssignment_1_1"
    // InternalFlow.g:1312:1: rule__Kind__NameAssignment_1_1 : ( RULE_STRING ) ;
    public final void rule__Kind__NameAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1316:1: ( ( RULE_STRING ) )
            // InternalFlow.g:1317:2: ( RULE_STRING )
            {
            // InternalFlow.g:1317:2: ( RULE_STRING )
            // InternalFlow.g:1318:3: RULE_STRING
            {
             before(grammarAccess.getKindAccess().getNameSTRINGTerminalRuleCall_1_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getKindAccess().getNameSTRINGTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Kind__NameAssignment_1_1"


    // $ANTLR start "rule__Medium__NameAssignment_1"
    // InternalFlow.g:1327:1: rule__Medium__NameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Medium__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFlow.g:1331:1: ( ( RULE_STRING ) )
            // InternalFlow.g:1332:2: ( RULE_STRING )
            {
            // InternalFlow.g:1332:2: ( RULE_STRING )
            // InternalFlow.g:1333:3: RULE_STRING
            {
             before(grammarAccess.getMediumAccess().getNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getMediumAccess().getNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Medium__NameAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000480000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000207800L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000007802L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000001800020L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000800020L});

}