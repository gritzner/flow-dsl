package de.uni_hannover.se.flow.dsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import de.uni_hannover.se.flow.dsl.services.FlowGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFlowParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'person'", "'domain'", "'task'", "'{'", "'}'", "'flows'", "'object'", "'by'", "'in'", "'out'", "'supportedBy'", "'controlledBy'", "'fluid'", "'solid'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalFlowParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFlowParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFlowParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFlow.g"; }



     	private FlowGrammarAccess grammarAccess;

        public InternalFlowParser(TokenStream input, FlowGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Interview";
       	}

       	@Override
       	protected FlowGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleInterview"
    // InternalFlow.g:65:1: entryRuleInterview returns [EObject current=null] : iv_ruleInterview= ruleInterview EOF ;
    public final EObject entryRuleInterview() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterview = null;


        try {
            // InternalFlow.g:65:50: (iv_ruleInterview= ruleInterview EOF )
            // InternalFlow.g:66:2: iv_ruleInterview= ruleInterview EOF
            {
             newCompositeNode(grammarAccess.getInterviewRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInterview=ruleInterview();

            state._fsp--;

             current =iv_ruleInterview; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterview"


    // $ANTLR start "ruleInterview"
    // InternalFlow.g:72:1: ruleInterview returns [EObject current=null] : ( ( (lv_domainModel_0_0= ruleImport ) ) otherlv_1= 'person' ( (otherlv_2= RULE_ID ) ) ( (lv_tasks_3_0= ruleTask ) )* ( (lv_flows_4_0= ruleFlows ) )? ) ;
    public final EObject ruleInterview() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_domainModel_0_0 = null;

        EObject lv_tasks_3_0 = null;

        EObject lv_flows_4_0 = null;



        	enterRule();

        try {
            // InternalFlow.g:78:2: ( ( ( (lv_domainModel_0_0= ruleImport ) ) otherlv_1= 'person' ( (otherlv_2= RULE_ID ) ) ( (lv_tasks_3_0= ruleTask ) )* ( (lv_flows_4_0= ruleFlows ) )? ) )
            // InternalFlow.g:79:2: ( ( (lv_domainModel_0_0= ruleImport ) ) otherlv_1= 'person' ( (otherlv_2= RULE_ID ) ) ( (lv_tasks_3_0= ruleTask ) )* ( (lv_flows_4_0= ruleFlows ) )? )
            {
            // InternalFlow.g:79:2: ( ( (lv_domainModel_0_0= ruleImport ) ) otherlv_1= 'person' ( (otherlv_2= RULE_ID ) ) ( (lv_tasks_3_0= ruleTask ) )* ( (lv_flows_4_0= ruleFlows ) )? )
            // InternalFlow.g:80:3: ( (lv_domainModel_0_0= ruleImport ) ) otherlv_1= 'person' ( (otherlv_2= RULE_ID ) ) ( (lv_tasks_3_0= ruleTask ) )* ( (lv_flows_4_0= ruleFlows ) )?
            {
            // InternalFlow.g:80:3: ( (lv_domainModel_0_0= ruleImport ) )
            // InternalFlow.g:81:4: (lv_domainModel_0_0= ruleImport )
            {
            // InternalFlow.g:81:4: (lv_domainModel_0_0= ruleImport )
            // InternalFlow.g:82:5: lv_domainModel_0_0= ruleImport
            {

            					newCompositeNode(grammarAccess.getInterviewAccess().getDomainModelImportParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_3);
            lv_domainModel_0_0=ruleImport();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getInterviewRule());
            					}
            					set(
            						current,
            						"domainModel",
            						lv_domainModel_0_0,
            						"de.uni_hannover.se.flow.dsl.Flow.Import");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getInterviewAccess().getPersonKeyword_1());
            		
            // InternalFlow.g:103:3: ( (otherlv_2= RULE_ID ) )
            // InternalFlow.g:104:4: (otherlv_2= RULE_ID )
            {
            // InternalFlow.g:104:4: (otherlv_2= RULE_ID )
            // InternalFlow.g:105:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getInterviewRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(otherlv_2, grammarAccess.getInterviewAccess().getPersonEClassCrossReference_2_0());
            				

            }


            }

            // InternalFlow.g:116:3: ( (lv_tasks_3_0= ruleTask ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==13) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalFlow.g:117:4: (lv_tasks_3_0= ruleTask )
            	    {
            	    // InternalFlow.g:117:4: (lv_tasks_3_0= ruleTask )
            	    // InternalFlow.g:118:5: lv_tasks_3_0= ruleTask
            	    {

            	    					newCompositeNode(grammarAccess.getInterviewAccess().getTasksTaskParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_tasks_3_0=ruleTask();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getInterviewRule());
            	    					}
            	    					add(
            	    						current,
            	    						"tasks",
            	    						lv_tasks_3_0,
            	    						"de.uni_hannover.se.flow.dsl.Flow.Task");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalFlow.g:135:3: ( (lv_flows_4_0= ruleFlows ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==16) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalFlow.g:136:4: (lv_flows_4_0= ruleFlows )
                    {
                    // InternalFlow.g:136:4: (lv_flows_4_0= ruleFlows )
                    // InternalFlow.g:137:5: lv_flows_4_0= ruleFlows
                    {

                    					newCompositeNode(grammarAccess.getInterviewAccess().getFlowsFlowsParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_flows_4_0=ruleFlows();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getInterviewRule());
                    					}
                    					set(
                    						current,
                    						"flows",
                    						lv_flows_4_0,
                    						"de.uni_hannover.se.flow.dsl.Flow.Flows");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterview"


    // $ANTLR start "entryRuleImport"
    // InternalFlow.g:158:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalFlow.g:158:47: (iv_ruleImport= ruleImport EOF )
            // InternalFlow.g:159:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalFlow.g:165:1: ruleImport returns [EObject current=null] : (otherlv_0= 'domain' ( (lv_uri_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_uri_1_0=null;


        	enterRule();

        try {
            // InternalFlow.g:171:2: ( (otherlv_0= 'domain' ( (lv_uri_1_0= RULE_STRING ) ) ) )
            // InternalFlow.g:172:2: (otherlv_0= 'domain' ( (lv_uri_1_0= RULE_STRING ) ) )
            {
            // InternalFlow.g:172:2: (otherlv_0= 'domain' ( (lv_uri_1_0= RULE_STRING ) ) )
            // InternalFlow.g:173:3: otherlv_0= 'domain' ( (lv_uri_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getDomainKeyword_0());
            		
            // InternalFlow.g:177:3: ( (lv_uri_1_0= RULE_STRING ) )
            // InternalFlow.g:178:4: (lv_uri_1_0= RULE_STRING )
            {
            // InternalFlow.g:178:4: (lv_uri_1_0= RULE_STRING )
            // InternalFlow.g:179:5: lv_uri_1_0= RULE_STRING
            {
            lv_uri_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_uri_1_0, grammarAccess.getImportAccess().getUriSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getImportRule());
            					}
            					setWithLastConsumed(
            						current,
            						"uri",
            						lv_uri_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleTask"
    // InternalFlow.g:199:1: entryRuleTask returns [EObject current=null] : iv_ruleTask= ruleTask EOF ;
    public final EObject entryRuleTask() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTask = null;


        try {
            // InternalFlow.g:199:45: (iv_ruleTask= ruleTask EOF )
            // InternalFlow.g:200:2: iv_ruleTask= ruleTask EOF
            {
             newCompositeNode(grammarAccess.getTaskRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTask=ruleTask();

            state._fsp--;

             current =iv_ruleTask; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTask"


    // $ANTLR start "ruleTask"
    // InternalFlow.g:206:1: ruleTask returns [EObject current=null] : (otherlv_0= 'task' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_flows_3_0= ruleFlow ) )* otherlv_4= '}' ) ;
    public final EObject ruleTask() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_flows_3_0 = null;



        	enterRule();

        try {
            // InternalFlow.g:212:2: ( (otherlv_0= 'task' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_flows_3_0= ruleFlow ) )* otherlv_4= '}' ) )
            // InternalFlow.g:213:2: (otherlv_0= 'task' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_flows_3_0= ruleFlow ) )* otherlv_4= '}' )
            {
            // InternalFlow.g:213:2: (otherlv_0= 'task' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_flows_3_0= ruleFlow ) )* otherlv_4= '}' )
            // InternalFlow.g:214:3: otherlv_0= 'task' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_flows_3_0= ruleFlow ) )* otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,13,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getTaskAccess().getTaskKeyword_0());
            		
            // InternalFlow.g:218:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalFlow.g:219:4: (lv_name_1_0= RULE_ID )
            {
            // InternalFlow.g:219:4: (lv_name_1_0= RULE_ID )
            // InternalFlow.g:220:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(lv_name_1_0, grammarAccess.getTaskAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTaskRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_2, grammarAccess.getTaskAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalFlow.g:240:3: ( (lv_flows_3_0= ruleFlow ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>=19 && LA3_0<=22)) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalFlow.g:241:4: (lv_flows_3_0= ruleFlow )
            	    {
            	    // InternalFlow.g:241:4: (lv_flows_3_0= ruleFlow )
            	    // InternalFlow.g:242:5: lv_flows_3_0= ruleFlow
            	    {

            	    					newCompositeNode(grammarAccess.getTaskAccess().getFlowsFlowParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_flows_3_0=ruleFlow();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTaskRule());
            	    					}
            	    					add(
            	    						current,
            	    						"flows",
            	    						lv_flows_3_0,
            	    						"de.uni_hannover.se.flow.dsl.Flow.Flow");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_4=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getTaskAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTask"


    // $ANTLR start "entryRuleFlows"
    // InternalFlow.g:267:1: entryRuleFlows returns [EObject current=null] : iv_ruleFlows= ruleFlows EOF ;
    public final EObject entryRuleFlows() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFlows = null;


        try {
            // InternalFlow.g:267:46: (iv_ruleFlows= ruleFlows EOF )
            // InternalFlow.g:268:2: iv_ruleFlows= ruleFlows EOF
            {
             newCompositeNode(grammarAccess.getFlowsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFlows=ruleFlows();

            state._fsp--;

             current =iv_ruleFlows; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFlows"


    // $ANTLR start "ruleFlows"
    // InternalFlow.g:274:1: ruleFlows returns [EObject current=null] : ( () otherlv_1= 'flows' otherlv_2= '{' ( (lv_flows_3_0= ruleFlow ) )* otherlv_4= '}' ) ;
    public final EObject ruleFlows() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_flows_3_0 = null;



        	enterRule();

        try {
            // InternalFlow.g:280:2: ( ( () otherlv_1= 'flows' otherlv_2= '{' ( (lv_flows_3_0= ruleFlow ) )* otherlv_4= '}' ) )
            // InternalFlow.g:281:2: ( () otherlv_1= 'flows' otherlv_2= '{' ( (lv_flows_3_0= ruleFlow ) )* otherlv_4= '}' )
            {
            // InternalFlow.g:281:2: ( () otherlv_1= 'flows' otherlv_2= '{' ( (lv_flows_3_0= ruleFlow ) )* otherlv_4= '}' )
            // InternalFlow.g:282:3: () otherlv_1= 'flows' otherlv_2= '{' ( (lv_flows_3_0= ruleFlow ) )* otherlv_4= '}'
            {
            // InternalFlow.g:282:3: ()
            // InternalFlow.g:283:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFlowsAccess().getFlowsAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,16,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getFlowsAccess().getFlowsKeyword_1());
            		
            otherlv_2=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_2, grammarAccess.getFlowsAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalFlow.g:297:3: ( (lv_flows_3_0= ruleFlow ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=19 && LA4_0<=22)) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalFlow.g:298:4: (lv_flows_3_0= ruleFlow )
            	    {
            	    // InternalFlow.g:298:4: (lv_flows_3_0= ruleFlow )
            	    // InternalFlow.g:299:5: lv_flows_3_0= ruleFlow
            	    {

            	    					newCompositeNode(grammarAccess.getFlowsAccess().getFlowsFlowParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_flows_3_0=ruleFlow();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFlowsRule());
            	    					}
            	    					add(
            	    						current,
            	    						"flows",
            	    						lv_flows_3_0,
            	    						"de.uni_hannover.se.flow.dsl.Flow.Flow");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_4=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getFlowsAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFlows"


    // $ANTLR start "entryRuleFlow"
    // InternalFlow.g:324:1: entryRuleFlow returns [EObject current=null] : iv_ruleFlow= ruleFlow EOF ;
    public final EObject entryRuleFlow() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFlow = null;


        try {
            // InternalFlow.g:324:45: (iv_ruleFlow= ruleFlow EOF )
            // InternalFlow.g:325:2: iv_ruleFlow= ruleFlow EOF
            {
             newCompositeNode(grammarAccess.getFlowRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFlow=ruleFlow();

            state._fsp--;

             current =iv_ruleFlow; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFlow"


    // $ANTLR start "ruleFlow"
    // InternalFlow.g:331:1: ruleFlow returns [EObject current=null] : ( ( (lv_direction_0_0= ruleDirection ) ) ( (otherlv_1= RULE_ID ) ) ( (lv_type_2_0= ruleType ) ) ( (lv_kind_3_0= ruleKind ) )? ( (lv_medium_4_0= ruleMedium ) )? ) ;
    public final EObject ruleFlow() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Enumerator lv_direction_0_0 = null;

        Enumerator lv_type_2_0 = null;

        EObject lv_kind_3_0 = null;

        EObject lv_medium_4_0 = null;



        	enterRule();

        try {
            // InternalFlow.g:337:2: ( ( ( (lv_direction_0_0= ruleDirection ) ) ( (otherlv_1= RULE_ID ) ) ( (lv_type_2_0= ruleType ) ) ( (lv_kind_3_0= ruleKind ) )? ( (lv_medium_4_0= ruleMedium ) )? ) )
            // InternalFlow.g:338:2: ( ( (lv_direction_0_0= ruleDirection ) ) ( (otherlv_1= RULE_ID ) ) ( (lv_type_2_0= ruleType ) ) ( (lv_kind_3_0= ruleKind ) )? ( (lv_medium_4_0= ruleMedium ) )? )
            {
            // InternalFlow.g:338:2: ( ( (lv_direction_0_0= ruleDirection ) ) ( (otherlv_1= RULE_ID ) ) ( (lv_type_2_0= ruleType ) ) ( (lv_kind_3_0= ruleKind ) )? ( (lv_medium_4_0= ruleMedium ) )? )
            // InternalFlow.g:339:3: ( (lv_direction_0_0= ruleDirection ) ) ( (otherlv_1= RULE_ID ) ) ( (lv_type_2_0= ruleType ) ) ( (lv_kind_3_0= ruleKind ) )? ( (lv_medium_4_0= ruleMedium ) )?
            {
            // InternalFlow.g:339:3: ( (lv_direction_0_0= ruleDirection ) )
            // InternalFlow.g:340:4: (lv_direction_0_0= ruleDirection )
            {
            // InternalFlow.g:340:4: (lv_direction_0_0= ruleDirection )
            // InternalFlow.g:341:5: lv_direction_0_0= ruleDirection
            {

            					newCompositeNode(grammarAccess.getFlowAccess().getDirectionDirectionEnumRuleCall_0_0());
            				
            pushFollow(FOLLOW_4);
            lv_direction_0_0=ruleDirection();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFlowRule());
            					}
            					set(
            						current,
            						"direction",
            						lv_direction_0_0,
            						"de.uni_hannover.se.flow.dsl.Flow.Direction");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalFlow.g:358:3: ( (otherlv_1= RULE_ID ) )
            // InternalFlow.g:359:4: (otherlv_1= RULE_ID )
            {
            // InternalFlow.g:359:4: (otherlv_1= RULE_ID )
            // InternalFlow.g:360:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFlowRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(otherlv_1, grammarAccess.getFlowAccess().getOtherEClassCrossReference_1_0());
            				

            }


            }

            // InternalFlow.g:371:3: ( (lv_type_2_0= ruleType ) )
            // InternalFlow.g:372:4: (lv_type_2_0= ruleType )
            {
            // InternalFlow.g:372:4: (lv_type_2_0= ruleType )
            // InternalFlow.g:373:5: lv_type_2_0= ruleType
            {

            					newCompositeNode(grammarAccess.getFlowAccess().getTypeTypeEnumRuleCall_2_0());
            				
            pushFollow(FOLLOW_10);
            lv_type_2_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFlowRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_2_0,
            						"de.uni_hannover.se.flow.dsl.Flow.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalFlow.g:390:3: ( (lv_kind_3_0= ruleKind ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_STRING||LA5_0==17) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalFlow.g:391:4: (lv_kind_3_0= ruleKind )
                    {
                    // InternalFlow.g:391:4: (lv_kind_3_0= ruleKind )
                    // InternalFlow.g:392:5: lv_kind_3_0= ruleKind
                    {

                    					newCompositeNode(grammarAccess.getFlowAccess().getKindKindParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_11);
                    lv_kind_3_0=ruleKind();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFlowRule());
                    					}
                    					set(
                    						current,
                    						"kind",
                    						lv_kind_3_0,
                    						"de.uni_hannover.se.flow.dsl.Flow.Kind");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalFlow.g:409:3: ( (lv_medium_4_0= ruleMedium ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==18) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalFlow.g:410:4: (lv_medium_4_0= ruleMedium )
                    {
                    // InternalFlow.g:410:4: (lv_medium_4_0= ruleMedium )
                    // InternalFlow.g:411:5: lv_medium_4_0= ruleMedium
                    {

                    					newCompositeNode(grammarAccess.getFlowAccess().getMediumMediumParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_medium_4_0=ruleMedium();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFlowRule());
                    					}
                    					set(
                    						current,
                    						"medium",
                    						lv_medium_4_0,
                    						"de.uni_hannover.se.flow.dsl.Flow.Medium");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFlow"


    // $ANTLR start "entryRuleKind"
    // InternalFlow.g:432:1: entryRuleKind returns [EObject current=null] : iv_ruleKind= ruleKind EOF ;
    public final EObject entryRuleKind() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleKind = null;


        try {
            // InternalFlow.g:432:45: (iv_ruleKind= ruleKind EOF )
            // InternalFlow.g:433:2: iv_ruleKind= ruleKind EOF
            {
             newCompositeNode(grammarAccess.getKindRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleKind=ruleKind();

            state._fsp--;

             current =iv_ruleKind; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleKind"


    // $ANTLR start "ruleKind"
    // InternalFlow.g:439:1: ruleKind returns [EObject current=null] : ( () ( (otherlv_1= 'object' ( (otherlv_2= RULE_ID ) ) ) | ( (lv_name_3_0= RULE_STRING ) ) ) ) ;
    public final EObject ruleKind() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;


        	enterRule();

        try {
            // InternalFlow.g:445:2: ( ( () ( (otherlv_1= 'object' ( (otherlv_2= RULE_ID ) ) ) | ( (lv_name_3_0= RULE_STRING ) ) ) ) )
            // InternalFlow.g:446:2: ( () ( (otherlv_1= 'object' ( (otherlv_2= RULE_ID ) ) ) | ( (lv_name_3_0= RULE_STRING ) ) ) )
            {
            // InternalFlow.g:446:2: ( () ( (otherlv_1= 'object' ( (otherlv_2= RULE_ID ) ) ) | ( (lv_name_3_0= RULE_STRING ) ) ) )
            // InternalFlow.g:447:3: () ( (otherlv_1= 'object' ( (otherlv_2= RULE_ID ) ) ) | ( (lv_name_3_0= RULE_STRING ) ) )
            {
            // InternalFlow.g:447:3: ()
            // InternalFlow.g:448:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getKindAccess().getKindAction_0(),
            					current);
            			

            }

            // InternalFlow.g:454:3: ( (otherlv_1= 'object' ( (otherlv_2= RULE_ID ) ) ) | ( (lv_name_3_0= RULE_STRING ) ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==17) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_STRING) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalFlow.g:455:4: (otherlv_1= 'object' ( (otherlv_2= RULE_ID ) ) )
                    {
                    // InternalFlow.g:455:4: (otherlv_1= 'object' ( (otherlv_2= RULE_ID ) ) )
                    // InternalFlow.g:456:5: otherlv_1= 'object' ( (otherlv_2= RULE_ID ) )
                    {
                    otherlv_1=(Token)match(input,17,FOLLOW_4); 

                    					newLeafNode(otherlv_1, grammarAccess.getKindAccess().getObjectKeyword_1_0_0());
                    				
                    // InternalFlow.g:460:5: ( (otherlv_2= RULE_ID ) )
                    // InternalFlow.g:461:6: (otherlv_2= RULE_ID )
                    {
                    // InternalFlow.g:461:6: (otherlv_2= RULE_ID )
                    // InternalFlow.g:462:7: otherlv_2= RULE_ID
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getKindRule());
                    							}
                    						
                    otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

                    							newLeafNode(otherlv_2, grammarAccess.getKindAccess().getObjectEClassCrossReference_1_0_1_0());
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalFlow.g:475:4: ( (lv_name_3_0= RULE_STRING ) )
                    {
                    // InternalFlow.g:475:4: ( (lv_name_3_0= RULE_STRING ) )
                    // InternalFlow.g:476:5: (lv_name_3_0= RULE_STRING )
                    {
                    // InternalFlow.g:476:5: (lv_name_3_0= RULE_STRING )
                    // InternalFlow.g:477:6: lv_name_3_0= RULE_STRING
                    {
                    lv_name_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_name_3_0, grammarAccess.getKindAccess().getNameSTRINGTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getKindRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"name",
                    							lv_name_3_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleKind"


    // $ANTLR start "entryRuleMedium"
    // InternalFlow.g:498:1: entryRuleMedium returns [EObject current=null] : iv_ruleMedium= ruleMedium EOF ;
    public final EObject entryRuleMedium() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMedium = null;


        try {
            // InternalFlow.g:498:47: (iv_ruleMedium= ruleMedium EOF )
            // InternalFlow.g:499:2: iv_ruleMedium= ruleMedium EOF
            {
             newCompositeNode(grammarAccess.getMediumRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMedium=ruleMedium();

            state._fsp--;

             current =iv_ruleMedium; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMedium"


    // $ANTLR start "ruleMedium"
    // InternalFlow.g:505:1: ruleMedium returns [EObject current=null] : (otherlv_0= 'by' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleMedium() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalFlow.g:511:2: ( (otherlv_0= 'by' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalFlow.g:512:2: (otherlv_0= 'by' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalFlow.g:512:2: (otherlv_0= 'by' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalFlow.g:513:3: otherlv_0= 'by' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getMediumAccess().getByKeyword_0());
            		
            // InternalFlow.g:517:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalFlow.g:518:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalFlow.g:518:4: (lv_name_1_0= RULE_STRING )
            // InternalFlow.g:519:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getMediumAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getMediumRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMedium"


    // $ANTLR start "ruleDirection"
    // InternalFlow.g:539:1: ruleDirection returns [Enumerator current=null] : ( (enumLiteral_0= 'in' ) | (enumLiteral_1= 'out' ) | (enumLiteral_2= 'supportedBy' ) | (enumLiteral_3= 'controlledBy' ) ) ;
    public final Enumerator ruleDirection() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalFlow.g:545:2: ( ( (enumLiteral_0= 'in' ) | (enumLiteral_1= 'out' ) | (enumLiteral_2= 'supportedBy' ) | (enumLiteral_3= 'controlledBy' ) ) )
            // InternalFlow.g:546:2: ( (enumLiteral_0= 'in' ) | (enumLiteral_1= 'out' ) | (enumLiteral_2= 'supportedBy' ) | (enumLiteral_3= 'controlledBy' ) )
            {
            // InternalFlow.g:546:2: ( (enumLiteral_0= 'in' ) | (enumLiteral_1= 'out' ) | (enumLiteral_2= 'supportedBy' ) | (enumLiteral_3= 'controlledBy' ) )
            int alt8=4;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt8=1;
                }
                break;
            case 20:
                {
                alt8=2;
                }
                break;
            case 21:
                {
                alt8=3;
                }
                break;
            case 22:
                {
                alt8=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalFlow.g:547:3: (enumLiteral_0= 'in' )
                    {
                    // InternalFlow.g:547:3: (enumLiteral_0= 'in' )
                    // InternalFlow.g:548:4: enumLiteral_0= 'in'
                    {
                    enumLiteral_0=(Token)match(input,19,FOLLOW_2); 

                    				current = grammarAccess.getDirectionAccess().getInEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDirectionAccess().getInEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFlow.g:555:3: (enumLiteral_1= 'out' )
                    {
                    // InternalFlow.g:555:3: (enumLiteral_1= 'out' )
                    // InternalFlow.g:556:4: enumLiteral_1= 'out'
                    {
                    enumLiteral_1=(Token)match(input,20,FOLLOW_2); 

                    				current = grammarAccess.getDirectionAccess().getOutEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDirectionAccess().getOutEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalFlow.g:563:3: (enumLiteral_2= 'supportedBy' )
                    {
                    // InternalFlow.g:563:3: (enumLiteral_2= 'supportedBy' )
                    // InternalFlow.g:564:4: enumLiteral_2= 'supportedBy'
                    {
                    enumLiteral_2=(Token)match(input,21,FOLLOW_2); 

                    				current = grammarAccess.getDirectionAccess().getSupportedByEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getDirectionAccess().getSupportedByEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalFlow.g:571:3: (enumLiteral_3= 'controlledBy' )
                    {
                    // InternalFlow.g:571:3: (enumLiteral_3= 'controlledBy' )
                    // InternalFlow.g:572:4: enumLiteral_3= 'controlledBy'
                    {
                    enumLiteral_3=(Token)match(input,22,FOLLOW_2); 

                    				current = grammarAccess.getDirectionAccess().getControlledByEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getDirectionAccess().getControlledByEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDirection"


    // $ANTLR start "ruleType"
    // InternalFlow.g:582:1: ruleType returns [Enumerator current=null] : ( (enumLiteral_0= 'fluid' ) | (enumLiteral_1= 'solid' ) ) ;
    public final Enumerator ruleType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalFlow.g:588:2: ( ( (enumLiteral_0= 'fluid' ) | (enumLiteral_1= 'solid' ) ) )
            // InternalFlow.g:589:2: ( (enumLiteral_0= 'fluid' ) | (enumLiteral_1= 'solid' ) )
            {
            // InternalFlow.g:589:2: ( (enumLiteral_0= 'fluid' ) | (enumLiteral_1= 'solid' ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==23) ) {
                alt9=1;
            }
            else if ( (LA9_0==24) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalFlow.g:590:3: (enumLiteral_0= 'fluid' )
                    {
                    // InternalFlow.g:590:3: (enumLiteral_0= 'fluid' )
                    // InternalFlow.g:591:4: enumLiteral_0= 'fluid'
                    {
                    enumLiteral_0=(Token)match(input,23,FOLLOW_2); 

                    				current = grammarAccess.getTypeAccess().getFluidEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getTypeAccess().getFluidEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFlow.g:598:3: (enumLiteral_1= 'solid' )
                    {
                    // InternalFlow.g:598:3: (enumLiteral_1= 'solid' )
                    // InternalFlow.g:599:4: enumLiteral_1= 'solid'
                    {
                    enumLiteral_1=(Token)match(input,24,FOLLOW_2); 

                    				current = grammarAccess.getTypeAccess().getSolidEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getTypeAccess().getSolidEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000012002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000788000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000001800000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000060022L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000040002L});

}