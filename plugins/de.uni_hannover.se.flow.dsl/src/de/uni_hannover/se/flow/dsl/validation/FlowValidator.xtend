// Copyright (c) 2017, Fachgebiet Software Engineerg, Leibniz Universitšt Hannover
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the Fachgebiet Software Engineering nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE FACHGEBIET SOFTWARE ENGINEERING BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package de.uni_hannover.se.flow.dsl.validation

import org.eclipse.xtext.validation.Check
import de.uni_hannover.se.flow.dsl.flow.Interview
import de.uni_hannover.se.flow.dsl.flow.FlowPackage
import de.uni_hannover.se.flow.dsl.utility.FlowHelper
import de.uni_hannover.se.flow.dsl.flow.Flow
import de.uni_hannover.se.flow.dsl.flow.Kind

/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class FlowValidator extends AbstractFlowValidator {
	
	@Check
	def checkDomainModelURIIsCorrect(Interview interview) {
		val model = FlowHelper.instance.getDomainModel(interview)
		if(model == null)
			error("invalid model reference", FlowPackage.Literals.INTERVIEW__DOMAIN_MODEL)
	} 
	
	@Check
	def checkPersonIsEClass(Interview interview) {
		val classes = FlowHelper.instance.getEClasses(interview)
		if(!classes.contains(interview.person))
			error("class not found", FlowPackage.Literals.INTERVIEW__PERSON, FlowIssueCodes.CLASS_NOT_FOUND)			
	}
	
	@Check
	def checkOtherIsEClass(Flow flow) {
		val classes = FlowHelper.instance.getEClasses(flow)
		if(!classes.contains(flow.other))
			error("class not found", FlowPackage.Literals.FLOW__OTHER, FlowIssueCodes.CLASS_NOT_FOUND)					
	}
	
	@Check
	def checkObjectIsEClass(Kind kind) {
		if(kind.object != null) {
			val classes = FlowHelper.instance.getEClasses(kind)
			if(!classes.contains(kind.object))
				error("class not found", FlowPackage.Literals.KIND__OBJECT, FlowIssueCodes.CLASS_NOT_FOUND)					
		}
	}
}
