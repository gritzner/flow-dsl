// Copyright (c) 2017, Fachgebiet Software Engineerg, Leibniz Universitšt Hannover
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the Fachgebiet Software Engineering nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE FACHGEBIET SOFTWARE ENGINEERING BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package de.uni_hannover.se.flow.dsl.scoping

import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.scoping.IScope
import de.uni_hannover.se.flow.dsl.flow.Interview
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider
import org.eclipse.xtext.scoping.Scopes
import de.uni_hannover.se.flow.dsl.flow.Flow
import de.uni_hannover.se.flow.dsl.utility.FlowHelper
import de.uni_hannover.se.flow.dsl.flow.Kind

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class FlowScopeProvider extends AbstractDeclarativeScopeProvider {
	def IScope scope_Interview_person(Interview interview, EReference ref) {
		return Scopes::scopeFor(FlowHelper.instance.getEClasses(interview)) 
	}
	
	def IScope scope_Flow_other(Flow flow, EReference ref) {
		return Scopes::scopeFor(FlowHelper.instance.getEClasses(flow)) 
	}
	
	def IScope scope_Kind_object(Kind kind, EReference ref) {
		return Scopes::scopeFor(FlowHelper.instance.getEClasses(kind)) 
	}
}
