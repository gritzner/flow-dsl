// Copyright (c) 2017, Fachgebiet Software Engineerg, Leibniz Universitšt Hannover
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the Fachgebiet Software Engineering nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE FACHGEBIET SOFTWARE ENGINEERING BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package de.uni_hannover.se.flow.dsl.utility

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.EObject
import de.uni_hannover.se.flow.dsl.flow.Interview
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.ecore.EClass
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.plugin.EcorePlugin
import org.eclipse.core.runtime.IPath

public class FlowHelper {
	public static val FlowHelper instance = new FlowHelper()
	
	private var ResourceSet resourceSet
	
	private new() {
		resourceSet = new ResourceSetImpl()
		resourceSet.getURIConverter().getURIMap().putAll(EcorePlugin.computePlatformURIMap(true))		
	}
	
	def Resource loadModel(IPath file) {
		return resourceSet.getResource(URI.createPlatformResourceURI(file.toString(), true), true);
	}

	def Resource getDomainModel(EObject obj) {
		try {
			var _interview = obj
			while(!(_interview instanceof Interview))
				_interview = _interview.eContainer
			val interview = _interview as Interview;

			val relativeModelUri = URI.createURI(interview.domainModel.uri)
			var absoluteModelUri = interview.eResource.getURI().trimSegments(1).appendSegments(relativeModelUri.segments)
			
			var URI modelUri = null
			for(var i = 1; i < absoluteModelUri.segmentCount; i+=1) {
				val newSegment =  absoluteModelUri.segment(i)
				if(!newSegment.equals("..")) {
					if(i == absoluteModelUri.segmentCount-1 || !absoluteModelUri.segment(i+1).equals("..")) { 
						if(modelUri != null) {
							modelUri = modelUri.appendSegment(newSegment)
						} else {
							modelUri = URI.createPlatformResourceURI(newSegment, false)
						}
					}
				}
			}
			
			return resourceSet.getResource(modelUri, true)
		} catch(Exception e) {
			return null	
		}
	}
	
	def static BasicEList<EClass> getEClasses(Resource model) {
		val classes = new BasicEList<EClass>()

		if(model != null) {
			val iter = model.allContents
			while(iter.hasNext) {
				val obj = iter.next
				if(obj instanceof EClass)
					classes.add(obj)
			}
		}		

		return classes		
	}
	
	def BasicEList<EClass> getEClasses(EObject obj) {
		return getEClasses(getDomainModel(obj))
	}
	
	def static String getAbsoluteFilePath(Resource res) {
		val root = ResourcesPlugin.workspace.root
		val proj = root.getProject(res.URI.segment(1))
		var uri = proj.locationURI.toString
		
		for(var i = 2; i < res.URI.segmentCount; i+=1) {
			uri = uri + "/" + res.URI.segment(i)
		}
		
		return uri
	}
}