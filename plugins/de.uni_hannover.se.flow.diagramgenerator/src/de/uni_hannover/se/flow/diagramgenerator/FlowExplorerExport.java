// Copyright (c) 2017, Fachgebiet Software Engineerg, Leibniz Universitšt Hannover
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the Fachgebiet Software Engineering nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE FACHGEBIET SOFTWARE ENGINEERING BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package de.uni_hannover.se.flow.diagramgenerator;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.uni_hannover.se.flow.dsl.utility.FlowHelper;
import flowdiagramm.Aktivitaet;
import flowdiagramm.Dokument;
import flowdiagramm.FlowDiagramm;
import flowdiagramm.Kante;
import flowdiagramm.Knoten;
import flowdiagramm.Person;
import flowdiagramm.PersonDokument;
/**
 * This class transforms the FLOW model.
 * @author Burak Kadioglu
 *
 */
public class FlowExplorerExport implements IObjectActionDelegate {
	/**
	 * This method transforms the FLOW model to the FLOW explorer's format.
	 * A xmi file will be generated.
	 */
	@Override
	public void run(IAction action) {
		
		IFile selectedFile = (IFile)((IStructuredSelection)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection()).getFirstElement();
		Resource xmiFile = FlowHelper.instance.loadModel(selectedFile.getFullPath()); //Ecore-Modell
		FlowDiagramm diagramm = (FlowDiagramm)xmiFile.getContents().get(0);
		
		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			Document document = builder.newDocument();
			document.setXmlStandalone(true);
			Element rootElement = document.createElement("process");
			document.appendChild(rootElement);
			Map<Knoten, String> nodes = new HashMap<>();
			
			int counter = 0;
			for(Knoten knoten : diagramm.getKnoten()) {
				if(knoten instanceof Person) {
					Element personElement = document.createElement("Person");
					rootElement.appendChild(personElement);
					String tmp = generateRandomID();
					nodes.put(knoten, tmp);
					personElement.setAttribute("id", tmp);
					personElement.setAttribute("text", knoten.getName());
					personElement.setAttribute("x", "" + (counter * 100));
					personElement.setAttribute("y", "0");
					personElement.setAttribute("isGroup", "false");
				} else if(knoten instanceof Dokument) {
					Element documentElement = document.createElement("Document");
					rootElement.appendChild(documentElement);					
					String tmp = generateRandomID();
					nodes.put(knoten, tmp);
					documentElement.setAttribute("id", tmp);
					documentElement.setAttribute("text", knoten.getName());
					documentElement.setAttribute("x", "" + (counter * 100));
					documentElement.setAttribute("y", "0");
					documentElement.setAttribute("isGroup", "false");
				} else if(knoten instanceof PersonDokument) {
					Element personDocumentElement = document.createElement("PersonDocument");
					rootElement.appendChild(personDocumentElement);
					String tmp = generateRandomID();
					nodes.put(knoten, tmp);
					personDocumentElement.setAttribute("id", tmp);
					personDocumentElement.setAttribute("text", knoten.getName());
					personDocumentElement.setAttribute("x", "" + (counter * 100));
					personDocumentElement.setAttribute("y", "0");
					personDocumentElement.setAttribute("isGroup", "false");
				} else if(knoten instanceof Aktivitaet) {
					Element activityElement = document.createElement("Activity");
					rootElement.appendChild(activityElement);
					String tmp = generateRandomID();
					nodes.put(knoten, tmp);
					activityElement.setAttribute("id", tmp);
					activityElement.setAttribute("text", knoten.getName());
					activityElement.setAttribute("x", "" + (counter * 100));
					activityElement.setAttribute("y", "0");
				}
				counter += 1;
			}
			for(Kante kante : diagramm.getKanten()) {
				if(kante.getVon() instanceof Person) {
					Element informationElement = document.createElement("Information");
					rootElement.appendChild(informationElement);
					informationElement.setAttribute("id", generateRandomID());
					informationElement.setAttribute("experience", ((Person) kante.getVon()).isIstExperte()+"");
					informationElement.setAttribute("fluid", "true");
					informationElement.setAttribute("sourceAnchor", null);
					informationElement.setAttribute("sourceID", nodes.get(kante.getVon()));
					informationElement.setAttribute("targetAnchor", setTargetAnchor(kante));
					informationElement.setAttribute("targetID", nodes.get(kante.getNach()));
					informationElement.setAttribute("wayPointsList", "0.0 0.0 , 0.0 0.0");	
				} else if(kante.getVon() instanceof Dokument) {
					Element informationElement = document.createElement("Information");
					rootElement.appendChild(informationElement);
					informationElement.setAttribute("id", generateRandomID());
					informationElement.setAttribute("experience", ((Dokument) kante.getVon()).isIstExperte()+"");
					informationElement.setAttribute("fluid", "false");
					informationElement.setAttribute("sourceAnchor", null);
					informationElement.setAttribute("sourceID", nodes.get(kante.getVon()));
					informationElement.setAttribute("targetAnchor", setTargetAnchor(kante));
					informationElement.setAttribute("targetID", nodes.get(kante.getNach()));
					informationElement.setAttribute("wayPointsList", "0.0 0.0 , 0.0 0.0");
				} else if(kante.getVon() instanceof PersonDokument) {
					Element informationElement = document.createElement("Information");
					rootElement.appendChild(informationElement);
					informationElement.setAttribute("id", generateRandomID());
					informationElement.setAttribute("experience", null);
					informationElement.setAttribute("fluid", "undefined");
					informationElement.setAttribute("sourceAnchor", null);
					informationElement.setAttribute("sourceID", nodes.get(kante.getVon()));
					informationElement.setAttribute("targetAnchor", setTargetAnchor(kante));
					informationElement.setAttribute("targetID", nodes.get(kante.getNach()));
					informationElement.setAttribute("wayPointsList", "0.0 0.0 , 0.0 0.0");
				} else if(kante.getVon() instanceof Aktivitaet) {
					Element informationElement = document.createElement("Information");
					rootElement.appendChild(informationElement);
					informationElement.setAttribute("id", generateRandomID());
					informationElement.setAttribute("experience", null);
					informationElement.setAttribute("fluid", "false");
					informationElement.setAttribute("sourceAnchor", "RIGHT");
					informationElement.setAttribute("sourceID", nodes.get(kante.getVon()));
					informationElement.setAttribute("targetAnchor", setTargetAnchor(kante));
					informationElement.setAttribute("targetID", nodes.get(kante.getNach()));
					informationElement.setAttribute("wayPointsList", "0.0 0.0 , 0.0 0.0");
				} 
			}
			TransformerFactory transFactory = TransformerFactory.newInstance();
			Transformer transformer = transFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			DOMSource domSource = new DOMSource(document);
			IPath fileName = selectedFile.getLocation().removeFileExtension().addFileExtension("xml");
			StreamResult result = new StreamResult(fileName.toFile());
			transformer.transform(domSource, result);
		} catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch(TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
	/**
	 * This method generates a random ID for each element of the FLOW model.
	 * @return ID of the element.
	 */
	private String generateRandomID() {
		Random r = new Random();
		final String alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
		final int n = alphabet.length();
		String output = "";
		for(int i = 0; i < 8; i++)
			output += alphabet.charAt(r.nextInt(n));
		output += "-";
		for(int i = 0; i < 4; i++)
			output += alphabet.charAt(r.nextInt(n));
		output += "-";
		for(int i = 0; i < 4; i++)
			output += alphabet.charAt(r.nextInt(n));
		output += "-";
		for(int i = 0; i < 4; i++)
			output += alphabet.charAt(r.nextInt(n));
		output += "-";
		for(int i = 0; i < 12; i++)
			output += alphabet.charAt(r.nextInt(n));
		return output;
	}
	/**
	 * This method sets the targetAnchor.
	 * @param kante row.
	 * @return String of the targetAnchor.
	 */
	private String setTargetAnchor(Kante kante) {
		if(kante.getNach() instanceof Aktivitaet) {
			Aktivitaet a = (Aktivitaet)kante.getNach();
			if(a.getUnterstuetzendeFlows().contains(kante)) 
				return "BOTTOM";
			else if(a.getKontrollierendeFlows().contains(kante))
				return "TOP";
			else 
				return "LEFT";
		}
		return "";
	}
	
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}
}
