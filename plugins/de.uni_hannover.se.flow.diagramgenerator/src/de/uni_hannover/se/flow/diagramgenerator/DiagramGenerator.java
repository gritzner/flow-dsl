// Copyright (c) 2017, Fachgebiet Software Engineerg, Leibniz Universit�t Hannover
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the Fachgebiet Software Engineering nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE FACHGEBIET SOFTWARE ENGINEERING BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package de.uni_hannover.se.flow.diagramgenerator;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import de.uni_hannover.se.flow.dsl.flow.Flow;
import de.uni_hannover.se.flow.dsl.flow.Interview;
import de.uni_hannover.se.flow.dsl.flow.Task;
import de.uni_hannover.se.flow.dsl.utility.FlowHelper;
import flowdiagramm.Aktivitaet;
import flowdiagramm.FlowDiagramm;
import flowdiagramm.FlowdiagrammFactory;
import flowdiagramm.Kante;
import flowdiagramm.Knoten;

/**
 * This class generates an instance of the FLOW meta model.
 * @author Burak Kadioglu
 *
 */
public class DiagramGenerator implements IObjectActionDelegate {
	/**
	 * This method iterates over all dsl files and creates a file that is an instance of the FLOW meta model.
	 */
	@Override
	public void run(IAction action) {
		
		IFile selectedFile = (IFile)((IStructuredSelection)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection()).getFirstElement();
		Resource model = FlowHelper.instance.loadModel(selectedFile.getFullPath()); //Ecore-Modell
		EList<EObject> objects = model.getContents().get(0).eContents();
		FlowDiagramm diagramm = FlowdiagrammFactory.eINSTANCE.createFlowDiagramm();	
		Set<Interview> interviews = new HashSet<Interview>(); 

		try {
			selectedFile.getProject().accept(new IResourceVisitor() {
				@Override
				public boolean visit(IResource resource) throws CoreException {
					String fileExtension = resource.getFileExtension();
					if(fileExtension == null || !fileExtension.equals("flow"))//Dateiendung .flow?
						return true;
					Resource flowModel = FlowHelper.instance.loadModel(resource.getFullPath());//Flow-Datei
					Interview interview = (Interview)flowModel.getContents().get(0);
					if(!objects.contains(interview.getPerson()))//Passt die Person zu dem akt. Ecore-Modell?
						return true; // interviewee part of a different set of interviews

					interviews.add(interview);
					
//					System.out.println("\nInterviewee: " + interview.getPerson().getName());
					Knoten knoten = FlowdiagrammFactory.eINSTANCE.createPerson();
					knoten.setName(interview.getPerson().getName());
					diagramm.getKnoten().add(knoten);
					return true;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Name -> Type
		Map<String, String> nameTypeMapping = new HashMap<String, String>(); 
		
		outer:
		for(EObject o : objects) {
			EStructuralFeature eStructuralFeature = o.eClass().getEStructuralFeature("name");
			String name = (String) o.eGet(eStructuralFeature);
			
			for(Knoten k : diagramm.getKnoten()) {
				if(k.getName().equals(name)) 
					continue outer;
			}

			boolean knotenErzeugt = false;
				
			for(Interview interview : interviews) {
				// Betrachte Tasks des Interviews
				if(interview.getTasks() != null) {
					for(Task task : interview.getTasks()) {
						for(Flow flow : task.getFlows()) {							
							if(flow.getOther().getName().equals(name)) {
								if( nameTypeMapping.containsKey(name)) {
									if(!nameTypeMapping.get(name).equals(flow.getType().getName())) {
										System.out.println("Something went wrong with: "+name);
									}
								} else { 
									switch(flow.getType()) {
										case FLUID: 
											Knoten kPerson = FlowdiagrammFactory.eINSTANCE.createPerson();
											kPerson.setName(name);	
											diagramm.getKnoten().add(kPerson);
											nameTypeMapping.put(name, "fluid");
											break;
										case SOLID: 
											Knoten kDokument = FlowdiagrammFactory.eINSTANCE.createDokument();
											kDokument.setName(name);
											diagramm.getKnoten().add(kDokument);
											nameTypeMapping.put(name, "solid");
											break;
										default:
											System.out.println("Type with value " + flow.getType().getName() + " not available!");
									}
								}							
							}
						}
					}
				} else {
					System.out.println("Interview hat keine Tasks!");
				}
				
				
				// Betrachte Flows des Interviews
				if(interview.getFlows() != null) {
					EList<Flow> flowList = interview.getFlows().getFlows();
					for(Flow flow : flowList) {						
						if(flow.getOther().getName().equals(name)) {
							if( nameTypeMapping.containsKey(name)) {
								if(!nameTypeMapping.get(name).equals(flow.getType().getName())) {
									System.out.println("Something went wrong with: "+name);
								}
							} else {
								switch(flow.getType()) {
									case FLUID:
										Knoten kPerson = FlowdiagrammFactory.eINSTANCE.createPerson();
										kPerson.setName(name);	
										diagramm.getKnoten().add(kPerson);
										nameTypeMapping.put(name, "fluid");
										break;
									case SOLID:
										Knoten kDokument = FlowdiagrammFactory.eINSTANCE.createDokument();
										kDokument.setName(name);
										diagramm.getKnoten().add(kDokument);
										nameTypeMapping.put(name, "solid");
										break;
									default:
										System.out.println("Type with value " + flow.getType().getName() + " not available!");
								}
							}							
						}
					}
				} else {
					System.out.println("Interview hat keine Flows!");
				}
				
				knotenErzeugt = true;
			}
			
			if(!knotenErzeugt) {
				Knoten kDokument = FlowdiagrammFactory.eINSTANCE.createPersonDokument();
				kDokument.setName(name);
				diagramm.getKnoten().add(kDokument);				
			}
		}
			
		for(Interview interview : interviews) {
			for (Task task : interview.getTasks()) {
				// Aktivitaeten
				Aktivitaet aktivitaet = null; 
				for (Knoten knoten : diagramm.getKnoten()) {
					if(knoten instanceof Aktivitaet) {
						if(knoten.getName() != null && task.getName() != null) {
							if(knoten.getName().equals(task.getName())) {
								aktivitaet = (Aktivitaet)knoten;
								aktivitaet.setName(task.getName());
							}	
						}	
					}
				}
				
				if(aktivitaet == null) {
//					// neue Aktivitaet erzeugen
					Knoten kAktivitaet = null;
					aktivitaet = (Aktivitaet)kAktivitaet;
					aktivitaet = FlowdiagrammFactory.eINSTANCE.createAktivitaet();
					aktivitaet.setName(task.getName());
					diagramm.getKnoten().add(aktivitaet);
				}
				
				for (Flow flow : task.getFlows()) {
					// Kanten zu Aktivitaet a
					Kante kante = FlowdiagrammFactory.eINSTANCE.createKante();
					Knoten other = null;
					for(Knoten k : diagramm.getKnoten()) {
						if(k.getName() != null)
							if(k.getName().equals(flow.getOther().getName()))
								other = k;
					}
					Assert.isTrue(other != null);
										
					switch(flow.getDirection()) {
					case IN:
						kante.setVon(other);
						kante.setNach(aktivitaet);
						aktivitaet.getAnkommendeFlows().add(kante);
						break;
					case OUT:
						kante.setVon(aktivitaet);
						kante.setNach(other);
						aktivitaet.getAusgehendeFlows().add(kante);
						break;
					case CONTROLLED_BY:
						kante.setVon(other);
						kante.setNach(aktivitaet);
						aktivitaet.getKontrollierendeFlows().add(kante);
						break;
					case SUPPORTED_BY:
						kante.setVon(other);
						kante.setNach(aktivitaet);
						aktivitaet.getUnterstuetzendeFlows().add(kante);
						break;
					}	
					
					// fluid/solid der Kante bestimmen {erst bei XMI nach XML machen}
//					Knoten k = kante.getVon();
//					if(k instanceof Aktivitaet) {
//						k = kante.getNach();
//					}
//					if(k instanceof Dokument) {
//						kante.setSolid();
//					} else {
//						kante.setFluid();
//					}
					diagramm.getKanten().add(kante);
				}
			}
			if(interview.getFlows() != null)
			for(Flow flow : interview.getFlows().getFlows()) {  //1.getflows gibt element zur�ck, 2.getflows gibt liste zur�ck
				Kante kante = FlowdiagrammFactory.eINSTANCE.createKante();
				Knoten other = null, interviewee = null;
				for(Knoten k : diagramm.getKnoten()) {
					if(k.getName().equals(interview.getPerson().getName()))
						interviewee = k;
					if(k.getName().equals(flow.getOther().getName()))
						other = k;
				}
				Assert.isTrue(other != null);
				
				switch(flow.getDirection()) {
				case IN:
					kante.setVon(other);
					kante.setNach(interviewee);
					break;
				case OUT:
					kante.setVon(interviewee);
					kante.setNach(other);
					break;
				case CONTROLLED_BY:
					kante.setVon(other);
					kante.setNach(interviewee);
					break;
				case SUPPORTED_BY:
					kante.setVon(other);
					kante.setNach(interviewee);
					break;
				}
				diagramm.getKanten().add(kante);
			}
		}
		
//		Diagramm via "Resource" speichern
		Resource.Factory rf = new XMIResourceFactoryImpl();
		Resource r = rf.createResource(URI.createFileURI(selectedFile.getFullPath().removeFileExtension().addFileExtension("xmi").toString()));
		r.getContents().add(diagramm);
		try {
			r.save(new HashMap <String, String>());
		} catch (IOException e) {
			System.out.println("Save Exception");
			e.printStackTrace();
		}
	}
	
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}
}
