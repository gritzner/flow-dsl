// Copyright (c) 2017, Fachgebiet Software Engineerg, Leibniz Universitšt Hannover
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the Fachgebiet Software Engineering nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE FACHGEBIET SOFTWARE ENGINEERING BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
package de.uni_hannover.se.flow.dsl.ui.quickfix

import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider
import de.uni_hannover.se.flow.dsl.validation.FlowIssueCodes
import org.eclipse.xtext.ui.editor.quickfix.Fix
import org.eclipse.xtext.validation.Issue
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.emf.ecore.EcoreFactory
import org.eclipse.emf.ecore.EPackage
import java.util.Collections
import de.uni_hannover.se.flow.dsl.utility.FlowHelper

/**
 * Custom quickfixes.
 *
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#quick-fixes
 */
class FlowQuickfixProvider extends DefaultQuickfixProvider {
	@Fix(FlowIssueCodes.CLASS_NOT_FOUND)
	def addClassToDomainModel(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(issue, "Add new class to model", "Add new class to model", null, new ISemanticModification() {
			override apply(EObject element, IModificationContext context) throws Exception {
				val model = FlowHelper.instance.getDomainModel(element)		
				if(model != null) {
					val theModel = model.contents.get(0) as EPackage
					val newClass = EcoreFactory.eINSTANCE.createEClass
					newClass.name = context.xtextDocument.get(issue.offset, issue.length)
					theModel.EClassifiers.add(newClass)
					model.save(Collections.EMPTY_MAP)
				}
			}
		})
	}
}
