/*
 * generated by Xtext 2.10.0
 */
package de.uni_hannover.se.flow.dsl.ui;

import com.google.inject.Injector;
import de.uni_hannover.se.flow.dsl.ui.internal.DslActivator;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class FlowExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return DslActivator.getInstance().getBundle();
	}
	
	@Override
	protected Injector getInjector() {
		return DslActivator.getInstance().getInjector(DslActivator.DE_UNI_HANNOVER_SE_FLOW_DSL_FLOW);
	}
	
}
