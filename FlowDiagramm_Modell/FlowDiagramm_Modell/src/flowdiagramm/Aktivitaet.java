/**
 */
package flowdiagramm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aktivitaet</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link flowdiagramm.Aktivitaet#getUnterstuetzendeFlows <em>Unterstuetzende Flows</em>}</li>
 *   <li>{@link flowdiagramm.Aktivitaet#getKontrollierendeFlows <em>Kontrollierende Flows</em>}</li>
 *   <li>{@link flowdiagramm.Aktivitaet#getAnkommendeFlows <em>Ankommende Flows</em>}</li>
 *   <li>{@link flowdiagramm.Aktivitaet#getAusgehendeFlows <em>Ausgehende Flows</em>}</li>
 * </ul>
 *
 * @see flowdiagramm.FlowdiagrammPackage#getAktivitaet()
 * @model
 * @generated
 */
public interface Aktivitaet extends Knoten {
	/**
	 * Returns the value of the '<em><b>Unterstuetzende Flows</b></em>' reference list.
	 * The list contents are of type {@link flowdiagramm.Kante}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unterstuetzende Flows</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unterstuetzende Flows</em>' reference list.
	 * @see flowdiagramm.FlowdiagrammPackage#getAktivitaet_UnterstuetzendeFlows()
	 * @model
	 * @generated
	 */
	EList<Kante> getUnterstuetzendeFlows();

	/**
	 * Returns the value of the '<em><b>Kontrollierende Flows</b></em>' reference list.
	 * The list contents are of type {@link flowdiagramm.Kante}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kontrollierende Flows</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kontrollierende Flows</em>' reference list.
	 * @see flowdiagramm.FlowdiagrammPackage#getAktivitaet_KontrollierendeFlows()
	 * @model
	 * @generated
	 */
	EList<Kante> getKontrollierendeFlows();

	/**
	 * Returns the value of the '<em><b>Ankommende Flows</b></em>' reference list.
	 * The list contents are of type {@link flowdiagramm.Kante}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ankommende Flows</em>' reference list.
	 * @see flowdiagramm.FlowdiagrammPackage#getAktivitaet_AnkommendeFlows()
	 * @model
	 * @generated
	 */
	EList<Kante> getAnkommendeFlows();

	/**
	 * Returns the value of the '<em><b>Ausgehende Flows</b></em>' reference list.
	 * The list contents are of type {@link flowdiagramm.Kante}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ausgehende Flows</em>' reference list.
	 * @see flowdiagramm.FlowdiagrammPackage#getAktivitaet_AusgehendeFlows()
	 * @model
	 * @generated
	 */
	EList<Kante> getAusgehendeFlows();

} // Aktivitaet
