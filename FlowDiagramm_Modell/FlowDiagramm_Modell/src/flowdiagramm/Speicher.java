/**
 */
package flowdiagramm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Speicher</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link flowdiagramm.Speicher#isIstGruppe <em>Ist Gruppe</em>}</li>
 * </ul>
 *
 * @see flowdiagramm.FlowdiagrammPackage#getSpeicher()
 * @model abstract="true"
 * @generated
 */
public interface Speicher extends Knoten {
	/**
	 * Returns the value of the '<em><b>Ist Gruppe</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ist Gruppe</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ist Gruppe</em>' attribute.
	 * @see #setIstGruppe(boolean)
	 * @see flowdiagramm.FlowdiagrammPackage#getSpeicher_IstGruppe()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isIstGruppe();

	/**
	 * Sets the value of the '{@link flowdiagramm.Speicher#isIstGruppe <em>Ist Gruppe</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ist Gruppe</em>' attribute.
	 * @see #isIstGruppe()
	 * @generated
	 */
	void setIstGruppe(boolean value);

} // Speicher
