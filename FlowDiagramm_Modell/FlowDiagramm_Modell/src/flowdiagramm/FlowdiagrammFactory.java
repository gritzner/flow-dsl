/**
 */
package flowdiagramm;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see flowdiagramm.FlowdiagrammPackage
 * @generated
 */
public interface FlowdiagrammFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FlowdiagrammFactory eINSTANCE = flowdiagramm.impl.FlowdiagrammFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Flow Diagramm</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Flow Diagramm</em>'.
	 * @generated
	 */
	FlowDiagramm createFlowDiagramm();

	/**
	 * Returns a new object of class '<em>Kante</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Kante</em>'.
	 * @generated
	 */
	Kante createKante();

	/**
	 * Returns a new object of class '<em>Aktivitaet</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aktivitaet</em>'.
	 * @generated
	 */
	Aktivitaet createAktivitaet();

	/**
	 * Returns a new object of class '<em>Person</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Person</em>'.
	 * @generated
	 */
	Person createPerson();

	/**
	 * Returns a new object of class '<em>Dokument</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dokument</em>'.
	 * @generated
	 */
	Dokument createDokument();

	/**
	 * Returns a new object of class '<em>Person Dokument</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Person Dokument</em>'.
	 * @generated
	 */
	PersonDokument createPersonDokument();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FlowdiagrammPackage getFlowdiagrammPackage();

} //FlowdiagrammFactory
