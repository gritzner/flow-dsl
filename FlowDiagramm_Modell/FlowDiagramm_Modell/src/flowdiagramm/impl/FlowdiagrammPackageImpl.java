/**
 */
package flowdiagramm.impl;

import flowdiagramm.Aktivitaet;
import flowdiagramm.Dokument;
import flowdiagramm.Element;
import flowdiagramm.FlowDiagramm;
import flowdiagramm.FlowdiagrammFactory;
import flowdiagramm.FlowdiagrammPackage;
import flowdiagramm.Kante;
import flowdiagramm.Knoten;
import flowdiagramm.Person;
import flowdiagramm.PersonDokument;
import flowdiagramm.Speicher;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FlowdiagrammPackageImpl extends EPackageImpl implements FlowdiagrammPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flowDiagrammEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass elementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass knotenEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass kanteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass speicherEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aktivitaetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dokumentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personDokumentEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see flowdiagramm.FlowdiagrammPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FlowdiagrammPackageImpl() {
		super(eNS_URI, FlowdiagrammFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link FlowdiagrammPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FlowdiagrammPackage init() {
		if (isInited) return (FlowdiagrammPackage)EPackage.Registry.INSTANCE.getEPackage(FlowdiagrammPackage.eNS_URI);

		// Obtain or create and register package
		FlowdiagrammPackageImpl theFlowdiagrammPackage = (FlowdiagrammPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof FlowdiagrammPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new FlowdiagrammPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theFlowdiagrammPackage.createPackageContents();

		// Initialize created meta-data
		theFlowdiagrammPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFlowdiagrammPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FlowdiagrammPackage.eNS_URI, theFlowdiagrammPackage);
		return theFlowdiagrammPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFlowDiagramm() {
		return flowDiagrammEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFlowDiagramm_Knoten() {
		return (EReference)flowDiagrammEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFlowDiagramm_Kanten() {
		return (EReference)flowDiagrammEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFlowDiagramm_Name() {
		return (EAttribute)flowDiagrammEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElement() {
		return elementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getElement_Name() {
		return (EAttribute)elementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKnoten() {
		return knotenEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKante() {
		return kanteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getKante_Von() {
		return (EReference)kanteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getKante_Nach() {
		return (EReference)kanteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSpeicher() {
		return speicherEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSpeicher_IstGruppe() {
		return (EAttribute)speicherEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAktivitaet() {
		return aktivitaetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAktivitaet_UnterstuetzendeFlows() {
		return (EReference)aktivitaetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAktivitaet_KontrollierendeFlows() {
		return (EReference)aktivitaetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAktivitaet_AnkommendeFlows() {
		return (EReference)aktivitaetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAktivitaet_AusgehendeFlows() {
		return (EReference)aktivitaetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerson() {
		return personEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerson_IstExperte() {
		return (EAttribute)personEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDokument() {
		return dokumentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDokument_IstExperte() {
		return (EAttribute)dokumentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPersonDokument() {
		return personDokumentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlowdiagrammFactory getFlowdiagrammFactory() {
		return (FlowdiagrammFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		flowDiagrammEClass = createEClass(FLOW_DIAGRAMM);
		createEReference(flowDiagrammEClass, FLOW_DIAGRAMM__KNOTEN);
		createEReference(flowDiagrammEClass, FLOW_DIAGRAMM__KANTEN);
		createEAttribute(flowDiagrammEClass, FLOW_DIAGRAMM__NAME);

		elementEClass = createEClass(ELEMENT);
		createEAttribute(elementEClass, ELEMENT__NAME);

		knotenEClass = createEClass(KNOTEN);

		kanteEClass = createEClass(KANTE);
		createEReference(kanteEClass, KANTE__VON);
		createEReference(kanteEClass, KANTE__NACH);

		speicherEClass = createEClass(SPEICHER);
		createEAttribute(speicherEClass, SPEICHER__IST_GRUPPE);

		aktivitaetEClass = createEClass(AKTIVITAET);
		createEReference(aktivitaetEClass, AKTIVITAET__UNTERSTUETZENDE_FLOWS);
		createEReference(aktivitaetEClass, AKTIVITAET__KONTROLLIERENDE_FLOWS);
		createEReference(aktivitaetEClass, AKTIVITAET__ANKOMMENDE_FLOWS);
		createEReference(aktivitaetEClass, AKTIVITAET__AUSGEHENDE_FLOWS);

		personEClass = createEClass(PERSON);
		createEAttribute(personEClass, PERSON__IST_EXPERTE);

		dokumentEClass = createEClass(DOKUMENT);
		createEAttribute(dokumentEClass, DOKUMENT__IST_EXPERTE);

		personDokumentEClass = createEClass(PERSON_DOKUMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		knotenEClass.getESuperTypes().add(this.getElement());
		kanteEClass.getESuperTypes().add(this.getElement());
		speicherEClass.getESuperTypes().add(this.getKnoten());
		aktivitaetEClass.getESuperTypes().add(this.getKnoten());
		personEClass.getESuperTypes().add(this.getSpeicher());
		dokumentEClass.getESuperTypes().add(this.getSpeicher());
		personDokumentEClass.getESuperTypes().add(this.getSpeicher());

		// Initialize classes, features, and operations; add parameters
		initEClass(flowDiagrammEClass, FlowDiagramm.class, "FlowDiagramm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFlowDiagramm_Knoten(), this.getKnoten(), null, "knoten", null, 0, -1, FlowDiagramm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFlowDiagramm_Kanten(), this.getKante(), null, "kanten", null, 0, -1, FlowDiagramm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFlowDiagramm_Name(), ecorePackage.getEString(), "name", null, 0, 1, FlowDiagramm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(elementEClass, Element.class, "Element", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, Element.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(knotenEClass, Knoten.class, "Knoten", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(kanteEClass, Kante.class, "Kante", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getKante_Von(), this.getKnoten(), null, "von", null, 0, 1, Kante.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getKante_Nach(), this.getKnoten(), null, "nach", null, 0, 1, Kante.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(speicherEClass, Speicher.class, "Speicher", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSpeicher_IstGruppe(), ecorePackage.getEBoolean(), "istGruppe", "false", 1, 1, Speicher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aktivitaetEClass, Aktivitaet.class, "Aktivitaet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAktivitaet_UnterstuetzendeFlows(), this.getKante(), null, "unterstuetzendeFlows", null, 0, -1, Aktivitaet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAktivitaet_KontrollierendeFlows(), this.getKante(), null, "kontrollierendeFlows", null, 0, -1, Aktivitaet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAktivitaet_AnkommendeFlows(), this.getKante(), null, "ankommendeFlows", null, 0, -1, Aktivitaet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAktivitaet_AusgehendeFlows(), this.getKante(), null, "ausgehendeFlows", null, 0, -1, Aktivitaet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personEClass, Person.class, "Person", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerson_IstExperte(), ecorePackage.getEBoolean(), "istExperte", "false", 1, 1, Person.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dokumentEClass, Dokument.class, "Dokument", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDokument_IstExperte(), ecorePackage.getEBoolean(), "istExperte", "false", 1, 1, Dokument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(personDokumentEClass, PersonDokument.class, "PersonDokument", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //FlowdiagrammPackageImpl
