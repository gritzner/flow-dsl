/**
 */
package flowdiagramm.impl;

import flowdiagramm.FlowdiagrammPackage;
import flowdiagramm.Knoten;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Knoten</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class KnotenImpl extends ElementImpl implements Knoten {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KnotenImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FlowdiagrammPackage.Literals.KNOTEN;
	}

} //KnotenImpl
