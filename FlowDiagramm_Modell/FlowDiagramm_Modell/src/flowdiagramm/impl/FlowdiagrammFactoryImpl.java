/**
 */
package flowdiagramm.impl;

import flowdiagramm.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FlowdiagrammFactoryImpl extends EFactoryImpl implements FlowdiagrammFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FlowdiagrammFactory init() {
		try {
			FlowdiagrammFactory theFlowdiagrammFactory = (FlowdiagrammFactory)EPackage.Registry.INSTANCE.getEFactory(FlowdiagrammPackage.eNS_URI);
			if (theFlowdiagrammFactory != null) {
				return theFlowdiagrammFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FlowdiagrammFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlowdiagrammFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FlowdiagrammPackage.FLOW_DIAGRAMM: return createFlowDiagramm();
			case FlowdiagrammPackage.KANTE: return createKante();
			case FlowdiagrammPackage.AKTIVITAET: return createAktivitaet();
			case FlowdiagrammPackage.PERSON: return createPerson();
			case FlowdiagrammPackage.DOKUMENT: return createDokument();
			case FlowdiagrammPackage.PERSON_DOKUMENT: return createPersonDokument();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlowDiagramm createFlowDiagramm() {
		FlowDiagrammImpl flowDiagramm = new FlowDiagrammImpl();
		return flowDiagramm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Kante createKante() {
		KanteImpl kante = new KanteImpl();
		return kante;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aktivitaet createAktivitaet() {
		AktivitaetImpl aktivitaet = new AktivitaetImpl();
		return aktivitaet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person createPerson() {
		PersonImpl person = new PersonImpl();
		return person;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dokument createDokument() {
		DokumentImpl dokument = new DokumentImpl();
		return dokument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonDokument createPersonDokument() {
		PersonDokumentImpl personDokument = new PersonDokumentImpl();
		return personDokument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlowdiagrammPackage getFlowdiagrammPackage() {
		return (FlowdiagrammPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FlowdiagrammPackage getPackage() {
		return FlowdiagrammPackage.eINSTANCE;
	}

} //FlowdiagrammFactoryImpl
