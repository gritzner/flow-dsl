/**
 */
package flowdiagramm.impl;

import flowdiagramm.Aktivitaet;
import flowdiagramm.FlowdiagrammPackage;
import flowdiagramm.Kante;
import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aktivitaet</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link flowdiagramm.impl.AktivitaetImpl#getUnterstuetzendeFlows <em>Unterstuetzende Flows</em>}</li>
 *   <li>{@link flowdiagramm.impl.AktivitaetImpl#getKontrollierendeFlows <em>Kontrollierende Flows</em>}</li>
 *   <li>{@link flowdiagramm.impl.AktivitaetImpl#getAnkommendeFlows <em>Ankommende Flows</em>}</li>
 *   <li>{@link flowdiagramm.impl.AktivitaetImpl#getAusgehendeFlows <em>Ausgehende Flows</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AktivitaetImpl extends KnotenImpl implements Aktivitaet {
	/**
	 * The cached value of the '{@link #getUnterstuetzendeFlows() <em>Unterstuetzende Flows</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnterstuetzendeFlows()
	 * @generated
	 * @ordered
	 */
	protected EList<Kante> unterstuetzendeFlows;

	/**
	 * The cached value of the '{@link #getKontrollierendeFlows() <em>Kontrollierende Flows</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKontrollierendeFlows()
	 * @generated
	 * @ordered
	 */
	protected EList<Kante> kontrollierendeFlows;

	/**
	 * The cached value of the '{@link #getAnkommendeFlows() <em>Ankommende Flows</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnkommendeFlows()
	 * @generated
	 * @ordered
	 */
	protected EList<Kante> ankommendeFlows;

	/**
	 * The cached value of the '{@link #getAusgehendeFlows() <em>Ausgehende Flows</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAusgehendeFlows()
	 * @generated
	 * @ordered
	 */
	protected EList<Kante> ausgehendeFlows;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AktivitaetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FlowdiagrammPackage.Literals.AKTIVITAET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Kante> getUnterstuetzendeFlows() {
		if (unterstuetzendeFlows == null) {
			unterstuetzendeFlows = new EObjectResolvingEList<Kante>(Kante.class, this, FlowdiagrammPackage.AKTIVITAET__UNTERSTUETZENDE_FLOWS);
		}
		return unterstuetzendeFlows;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Kante> getKontrollierendeFlows() {
		if (kontrollierendeFlows == null) {
			kontrollierendeFlows = new EObjectResolvingEList<Kante>(Kante.class, this, FlowdiagrammPackage.AKTIVITAET__KONTROLLIERENDE_FLOWS);
		}
		return kontrollierendeFlows;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Kante> getAnkommendeFlows() {
		if (ankommendeFlows == null) {
			ankommendeFlows = new EObjectResolvingEList<Kante>(Kante.class, this, FlowdiagrammPackage.AKTIVITAET__ANKOMMENDE_FLOWS);
		}
		return ankommendeFlows;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Kante> getAusgehendeFlows() {
		if (ausgehendeFlows == null) {
			ausgehendeFlows = new EObjectResolvingEList<Kante>(Kante.class, this, FlowdiagrammPackage.AKTIVITAET__AUSGEHENDE_FLOWS);
		}
		return ausgehendeFlows;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FlowdiagrammPackage.AKTIVITAET__UNTERSTUETZENDE_FLOWS:
				return getUnterstuetzendeFlows();
			case FlowdiagrammPackage.AKTIVITAET__KONTROLLIERENDE_FLOWS:
				return getKontrollierendeFlows();
			case FlowdiagrammPackage.AKTIVITAET__ANKOMMENDE_FLOWS:
				return getAnkommendeFlows();
			case FlowdiagrammPackage.AKTIVITAET__AUSGEHENDE_FLOWS:
				return getAusgehendeFlows();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FlowdiagrammPackage.AKTIVITAET__UNTERSTUETZENDE_FLOWS:
				getUnterstuetzendeFlows().clear();
				getUnterstuetzendeFlows().addAll((Collection<? extends Kante>)newValue);
				return;
			case FlowdiagrammPackage.AKTIVITAET__KONTROLLIERENDE_FLOWS:
				getKontrollierendeFlows().clear();
				getKontrollierendeFlows().addAll((Collection<? extends Kante>)newValue);
				return;
			case FlowdiagrammPackage.AKTIVITAET__ANKOMMENDE_FLOWS:
				getAnkommendeFlows().clear();
				getAnkommendeFlows().addAll((Collection<? extends Kante>)newValue);
				return;
			case FlowdiagrammPackage.AKTIVITAET__AUSGEHENDE_FLOWS:
				getAusgehendeFlows().clear();
				getAusgehendeFlows().addAll((Collection<? extends Kante>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FlowdiagrammPackage.AKTIVITAET__UNTERSTUETZENDE_FLOWS:
				getUnterstuetzendeFlows().clear();
				return;
			case FlowdiagrammPackage.AKTIVITAET__KONTROLLIERENDE_FLOWS:
				getKontrollierendeFlows().clear();
				return;
			case FlowdiagrammPackage.AKTIVITAET__ANKOMMENDE_FLOWS:
				getAnkommendeFlows().clear();
				return;
			case FlowdiagrammPackage.AKTIVITAET__AUSGEHENDE_FLOWS:
				getAusgehendeFlows().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FlowdiagrammPackage.AKTIVITAET__UNTERSTUETZENDE_FLOWS:
				return unterstuetzendeFlows != null && !unterstuetzendeFlows.isEmpty();
			case FlowdiagrammPackage.AKTIVITAET__KONTROLLIERENDE_FLOWS:
				return kontrollierendeFlows != null && !kontrollierendeFlows.isEmpty();
			case FlowdiagrammPackage.AKTIVITAET__ANKOMMENDE_FLOWS:
				return ankommendeFlows != null && !ankommendeFlows.isEmpty();
			case FlowdiagrammPackage.AKTIVITAET__AUSGEHENDE_FLOWS:
				return ausgehendeFlows != null && !ausgehendeFlows.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AktivitaetImpl
