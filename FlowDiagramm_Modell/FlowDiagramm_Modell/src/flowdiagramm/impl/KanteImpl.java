/**
 */
package flowdiagramm.impl;

import flowdiagramm.FlowdiagrammPackage;
import flowdiagramm.Kante;
import flowdiagramm.Knoten;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Kante</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link flowdiagramm.impl.KanteImpl#getVon <em>Von</em>}</li>
 *   <li>{@link flowdiagramm.impl.KanteImpl#getNach <em>Nach</em>}</li>
 * </ul>
 *
 * @generated
 */
public class KanteImpl extends ElementImpl implements Kante {
	/**
	 * The cached value of the '{@link #getVon() <em>Von</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVon()
	 * @generated
	 * @ordered
	 */
	protected Knoten von;

	/**
	 * The cached value of the '{@link #getNach() <em>Nach</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNach()
	 * @generated
	 * @ordered
	 */
	protected Knoten nach;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KanteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FlowdiagrammPackage.Literals.KANTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Knoten getVon() {
		if (von != null && von.eIsProxy()) {
			InternalEObject oldVon = (InternalEObject)von;
			von = (Knoten)eResolveProxy(oldVon);
			if (von != oldVon) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FlowdiagrammPackage.KANTE__VON, oldVon, von));
			}
		}
		return von;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Knoten basicGetVon() {
		return von;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVon(Knoten newVon) {
		Knoten oldVon = von;
		von = newVon;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FlowdiagrammPackage.KANTE__VON, oldVon, von));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Knoten getNach() {
		if (nach != null && nach.eIsProxy()) {
			InternalEObject oldNach = (InternalEObject)nach;
			nach = (Knoten)eResolveProxy(oldNach);
			if (nach != oldNach) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FlowdiagrammPackage.KANTE__NACH, oldNach, nach));
			}
		}
		return nach;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Knoten basicGetNach() {
		return nach;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNach(Knoten newNach) {
		Knoten oldNach = nach;
		nach = newNach;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FlowdiagrammPackage.KANTE__NACH, oldNach, nach));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FlowdiagrammPackage.KANTE__VON:
				if (resolve) return getVon();
				return basicGetVon();
			case FlowdiagrammPackage.KANTE__NACH:
				if (resolve) return getNach();
				return basicGetNach();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FlowdiagrammPackage.KANTE__VON:
				setVon((Knoten)newValue);
				return;
			case FlowdiagrammPackage.KANTE__NACH:
				setNach((Knoten)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FlowdiagrammPackage.KANTE__VON:
				setVon((Knoten)null);
				return;
			case FlowdiagrammPackage.KANTE__NACH:
				setNach((Knoten)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FlowdiagrammPackage.KANTE__VON:
				return von != null;
			case FlowdiagrammPackage.KANTE__NACH:
				return nach != null;
		}
		return super.eIsSet(featureID);
	}

} //KanteImpl
