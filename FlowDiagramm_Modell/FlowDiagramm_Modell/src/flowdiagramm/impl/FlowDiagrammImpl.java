/**
 */
package flowdiagramm.impl;

import flowdiagramm.FlowDiagramm;
import flowdiagramm.FlowdiagrammPackage;
import flowdiagramm.Kante;
import flowdiagramm.Knoten;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Flow Diagramm</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link flowdiagramm.impl.FlowDiagrammImpl#getKnoten <em>Knoten</em>}</li>
 *   <li>{@link flowdiagramm.impl.FlowDiagrammImpl#getKanten <em>Kanten</em>}</li>
 *   <li>{@link flowdiagramm.impl.FlowDiagrammImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FlowDiagrammImpl extends MinimalEObjectImpl.Container implements FlowDiagramm {
	/**
	 * The cached value of the '{@link #getKnoten() <em>Knoten</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKnoten()
	 * @generated
	 * @ordered
	 */
	protected EList<Knoten> knoten;

	/**
	 * The cached value of the '{@link #getKanten() <em>Kanten</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKanten()
	 * @generated
	 * @ordered
	 */
	protected EList<Kante> kanten;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FlowDiagrammImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FlowdiagrammPackage.Literals.FLOW_DIAGRAMM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Knoten> getKnoten() {
		if (knoten == null) {
			knoten = new EObjectContainmentEList<Knoten>(Knoten.class, this, FlowdiagrammPackage.FLOW_DIAGRAMM__KNOTEN);
		}
		return knoten;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Kante> getKanten() {
		if (kanten == null) {
			kanten = new EObjectContainmentEList<Kante>(Kante.class, this, FlowdiagrammPackage.FLOW_DIAGRAMM__KANTEN);
		}
		return kanten;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FlowdiagrammPackage.FLOW_DIAGRAMM__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FlowdiagrammPackage.FLOW_DIAGRAMM__KNOTEN:
				return ((InternalEList<?>)getKnoten()).basicRemove(otherEnd, msgs);
			case FlowdiagrammPackage.FLOW_DIAGRAMM__KANTEN:
				return ((InternalEList<?>)getKanten()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FlowdiagrammPackage.FLOW_DIAGRAMM__KNOTEN:
				return getKnoten();
			case FlowdiagrammPackage.FLOW_DIAGRAMM__KANTEN:
				return getKanten();
			case FlowdiagrammPackage.FLOW_DIAGRAMM__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FlowdiagrammPackage.FLOW_DIAGRAMM__KNOTEN:
				getKnoten().clear();
				getKnoten().addAll((Collection<? extends Knoten>)newValue);
				return;
			case FlowdiagrammPackage.FLOW_DIAGRAMM__KANTEN:
				getKanten().clear();
				getKanten().addAll((Collection<? extends Kante>)newValue);
				return;
			case FlowdiagrammPackage.FLOW_DIAGRAMM__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FlowdiagrammPackage.FLOW_DIAGRAMM__KNOTEN:
				getKnoten().clear();
				return;
			case FlowdiagrammPackage.FLOW_DIAGRAMM__KANTEN:
				getKanten().clear();
				return;
			case FlowdiagrammPackage.FLOW_DIAGRAMM__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FlowdiagrammPackage.FLOW_DIAGRAMM__KNOTEN:
				return knoten != null && !knoten.isEmpty();
			case FlowdiagrammPackage.FLOW_DIAGRAMM__KANTEN:
				return kanten != null && !kanten.isEmpty();
			case FlowdiagrammPackage.FLOW_DIAGRAMM__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //FlowDiagrammImpl
