/**
 */
package flowdiagramm.impl;

import flowdiagramm.FlowdiagrammPackage;
import flowdiagramm.PersonDokument;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Person Dokument</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PersonDokumentImpl extends SpeicherImpl implements PersonDokument {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonDokumentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FlowdiagrammPackage.Literals.PERSON_DOKUMENT;
	}

} //PersonDokumentImpl
