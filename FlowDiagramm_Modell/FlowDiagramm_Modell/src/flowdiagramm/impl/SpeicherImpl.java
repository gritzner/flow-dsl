/**
 */
package flowdiagramm.impl;

import flowdiagramm.FlowdiagrammPackage;
import flowdiagramm.Speicher;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Speicher</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link flowdiagramm.impl.SpeicherImpl#isIstGruppe <em>Ist Gruppe</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class SpeicherImpl extends KnotenImpl implements Speicher {
	/**
	 * The default value of the '{@link #isIstGruppe() <em>Ist Gruppe</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIstGruppe()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IST_GRUPPE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIstGruppe() <em>Ist Gruppe</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIstGruppe()
	 * @generated
	 * @ordered
	 */
	protected boolean istGruppe = IST_GRUPPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpeicherImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FlowdiagrammPackage.Literals.SPEICHER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIstGruppe() {
		return istGruppe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIstGruppe(boolean newIstGruppe) {
		boolean oldIstGruppe = istGruppe;
		istGruppe = newIstGruppe;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FlowdiagrammPackage.SPEICHER__IST_GRUPPE, oldIstGruppe, istGruppe));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FlowdiagrammPackage.SPEICHER__IST_GRUPPE:
				return isIstGruppe();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FlowdiagrammPackage.SPEICHER__IST_GRUPPE:
				setIstGruppe((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FlowdiagrammPackage.SPEICHER__IST_GRUPPE:
				setIstGruppe(IST_GRUPPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FlowdiagrammPackage.SPEICHER__IST_GRUPPE:
				return istGruppe != IST_GRUPPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (istGruppe: ");
		result.append(istGruppe);
		result.append(')');
		return result.toString();
	}

} //SpeicherImpl
