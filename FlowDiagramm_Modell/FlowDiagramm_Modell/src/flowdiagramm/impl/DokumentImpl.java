/**
 */
package flowdiagramm.impl;

import flowdiagramm.Dokument;
import flowdiagramm.FlowdiagrammPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dokument</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link flowdiagramm.impl.DokumentImpl#isIstExperte <em>Ist Experte</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DokumentImpl extends SpeicherImpl implements Dokument {
	/**
	 * The default value of the '{@link #isIstExperte() <em>Ist Experte</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIstExperte()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IST_EXPERTE_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isIstExperte() <em>Ist Experte</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIstExperte()
	 * @generated
	 * @ordered
	 */
	protected boolean istExperte = IST_EXPERTE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DokumentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FlowdiagrammPackage.Literals.DOKUMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIstExperte() {
		return istExperte;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIstExperte(boolean newIstExperte) {
		boolean oldIstExperte = istExperte;
		istExperte = newIstExperte;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FlowdiagrammPackage.DOKUMENT__IST_EXPERTE, oldIstExperte, istExperte));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FlowdiagrammPackage.DOKUMENT__IST_EXPERTE:
				return isIstExperte();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FlowdiagrammPackage.DOKUMENT__IST_EXPERTE:
				setIstExperte((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FlowdiagrammPackage.DOKUMENT__IST_EXPERTE:
				setIstExperte(IST_EXPERTE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FlowdiagrammPackage.DOKUMENT__IST_EXPERTE:
				return istExperte != IST_EXPERTE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (istExperte: ");
		result.append(istExperte);
		result.append(')');
		return result.toString();
	}

} //DokumentImpl
