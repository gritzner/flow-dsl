/**
 */
package flowdiagramm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flow Diagramm</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link flowdiagramm.FlowDiagramm#getKnoten <em>Knoten</em>}</li>
 *   <li>{@link flowdiagramm.FlowDiagramm#getKanten <em>Kanten</em>}</li>
 *   <li>{@link flowdiagramm.FlowDiagramm#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see flowdiagramm.FlowdiagrammPackage#getFlowDiagramm()
 * @model
 * @generated
 */
public interface FlowDiagramm extends EObject {
	/**
	 * Returns the value of the '<em><b>Knoten</b></em>' containment reference list.
	 * The list contents are of type {@link flowdiagramm.Knoten}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Knoten</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Knoten</em>' containment reference list.
	 * @see flowdiagramm.FlowdiagrammPackage#getFlowDiagramm_Knoten()
	 * @model containment="true"
	 * @generated
	 */
	EList<Knoten> getKnoten();

	/**
	 * Returns the value of the '<em><b>Kanten</b></em>' containment reference list.
	 * The list contents are of type {@link flowdiagramm.Kante}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kanten</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kanten</em>' containment reference list.
	 * @see flowdiagramm.FlowdiagrammPackage#getFlowDiagramm_Kanten()
	 * @model containment="true"
	 * @generated
	 */
	EList<Kante> getKanten();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see flowdiagramm.FlowdiagrammPackage#getFlowDiagramm_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link flowdiagramm.FlowDiagramm#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // FlowDiagramm
