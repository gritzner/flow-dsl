/**
 */
package flowdiagramm;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see flowdiagramm.FlowdiagrammFactory
 * @model kind="package"
 * @generated
 */
public interface FlowdiagrammPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "flowdiagramm";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.uni_hannover.de/se/flow/diagramm";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "flowdiagramm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FlowdiagrammPackage eINSTANCE = flowdiagramm.impl.FlowdiagrammPackageImpl.init();

	/**
	 * The meta object id for the '{@link flowdiagramm.impl.FlowDiagrammImpl <em>Flow Diagramm</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see flowdiagramm.impl.FlowDiagrammImpl
	 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getFlowDiagramm()
	 * @generated
	 */
	int FLOW_DIAGRAMM = 0;

	/**
	 * The feature id for the '<em><b>Knoten</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_DIAGRAMM__KNOTEN = 0;

	/**
	 * The feature id for the '<em><b>Kanten</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_DIAGRAMM__KANTEN = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_DIAGRAMM__NAME = 2;

	/**
	 * The number of structural features of the '<em>Flow Diagramm</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_DIAGRAMM_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Flow Diagramm</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOW_DIAGRAMM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link flowdiagramm.impl.ElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see flowdiagramm.impl.ElementImpl
	 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getElement()
	 * @generated
	 */
	int ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link flowdiagramm.impl.KnotenImpl <em>Knoten</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see flowdiagramm.impl.KnotenImpl
	 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getKnoten()
	 * @generated
	 */
	int KNOTEN = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KNOTEN__NAME = ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Knoten</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KNOTEN_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Knoten</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KNOTEN_OPERATION_COUNT = ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link flowdiagramm.impl.KanteImpl <em>Kante</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see flowdiagramm.impl.KanteImpl
	 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getKante()
	 * @generated
	 */
	int KANTE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KANTE__NAME = ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Von</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KANTE__VON = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nach</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KANTE__NACH = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Kante</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KANTE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Kante</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KANTE_OPERATION_COUNT = ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link flowdiagramm.impl.SpeicherImpl <em>Speicher</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see flowdiagramm.impl.SpeicherImpl
	 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getSpeicher()
	 * @generated
	 */
	int SPEICHER = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPEICHER__NAME = KNOTEN__NAME;

	/**
	 * The feature id for the '<em><b>Ist Gruppe</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPEICHER__IST_GRUPPE = KNOTEN_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Speicher</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPEICHER_FEATURE_COUNT = KNOTEN_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Speicher</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPEICHER_OPERATION_COUNT = KNOTEN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link flowdiagramm.impl.AktivitaetImpl <em>Aktivitaet</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see flowdiagramm.impl.AktivitaetImpl
	 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getAktivitaet()
	 * @generated
	 */
	int AKTIVITAET = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AKTIVITAET__NAME = KNOTEN__NAME;

	/**
	 * The feature id for the '<em><b>Unterstuetzende Flows</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AKTIVITAET__UNTERSTUETZENDE_FLOWS = KNOTEN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Kontrollierende Flows</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AKTIVITAET__KONTROLLIERENDE_FLOWS = KNOTEN_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ankommende Flows</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AKTIVITAET__ANKOMMENDE_FLOWS = KNOTEN_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Ausgehende Flows</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AKTIVITAET__AUSGEHENDE_FLOWS = KNOTEN_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Aktivitaet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AKTIVITAET_FEATURE_COUNT = KNOTEN_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Aktivitaet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AKTIVITAET_OPERATION_COUNT = KNOTEN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link flowdiagramm.impl.PersonImpl <em>Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see flowdiagramm.impl.PersonImpl
	 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getPerson()
	 * @generated
	 */
	int PERSON = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__NAME = SPEICHER__NAME;

	/**
	 * The feature id for the '<em><b>Ist Gruppe</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__IST_GRUPPE = SPEICHER__IST_GRUPPE;

	/**
	 * The feature id for the '<em><b>Ist Experte</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__IST_EXPERTE = SPEICHER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_FEATURE_COUNT = SPEICHER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_OPERATION_COUNT = SPEICHER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link flowdiagramm.impl.DokumentImpl <em>Dokument</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see flowdiagramm.impl.DokumentImpl
	 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getDokument()
	 * @generated
	 */
	int DOKUMENT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOKUMENT__NAME = SPEICHER__NAME;

	/**
	 * The feature id for the '<em><b>Ist Gruppe</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOKUMENT__IST_GRUPPE = SPEICHER__IST_GRUPPE;

	/**
	 * The feature id for the '<em><b>Ist Experte</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOKUMENT__IST_EXPERTE = SPEICHER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dokument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOKUMENT_FEATURE_COUNT = SPEICHER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Dokument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOKUMENT_OPERATION_COUNT = SPEICHER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link flowdiagramm.impl.PersonDokumentImpl <em>Person Dokument</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see flowdiagramm.impl.PersonDokumentImpl
	 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getPersonDokument()
	 * @generated
	 */
	int PERSON_DOKUMENT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_DOKUMENT__NAME = SPEICHER__NAME;

	/**
	 * The feature id for the '<em><b>Ist Gruppe</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_DOKUMENT__IST_GRUPPE = SPEICHER__IST_GRUPPE;

	/**
	 * The number of structural features of the '<em>Person Dokument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_DOKUMENT_FEATURE_COUNT = SPEICHER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Person Dokument</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_DOKUMENT_OPERATION_COUNT = SPEICHER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link flowdiagramm.FlowDiagramm <em>Flow Diagramm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flow Diagramm</em>'.
	 * @see flowdiagramm.FlowDiagramm
	 * @generated
	 */
	EClass getFlowDiagramm();

	/**
	 * Returns the meta object for the containment reference list '{@link flowdiagramm.FlowDiagramm#getKnoten <em>Knoten</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Knoten</em>'.
	 * @see flowdiagramm.FlowDiagramm#getKnoten()
	 * @see #getFlowDiagramm()
	 * @generated
	 */
	EReference getFlowDiagramm_Knoten();

	/**
	 * Returns the meta object for the containment reference list '{@link flowdiagramm.FlowDiagramm#getKanten <em>Kanten</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Kanten</em>'.
	 * @see flowdiagramm.FlowDiagramm#getKanten()
	 * @see #getFlowDiagramm()
	 * @generated
	 */
	EReference getFlowDiagramm_Kanten();

	/**
	 * Returns the meta object for the attribute '{@link flowdiagramm.FlowDiagramm#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see flowdiagramm.FlowDiagramm#getName()
	 * @see #getFlowDiagramm()
	 * @generated
	 */
	EAttribute getFlowDiagramm_Name();

	/**
	 * Returns the meta object for class '{@link flowdiagramm.Element <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see flowdiagramm.Element
	 * @generated
	 */
	EClass getElement();

	/**
	 * Returns the meta object for the attribute '{@link flowdiagramm.Element#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see flowdiagramm.Element#getName()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Name();

	/**
	 * Returns the meta object for class '{@link flowdiagramm.Knoten <em>Knoten</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Knoten</em>'.
	 * @see flowdiagramm.Knoten
	 * @generated
	 */
	EClass getKnoten();

	/**
	 * Returns the meta object for class '{@link flowdiagramm.Kante <em>Kante</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Kante</em>'.
	 * @see flowdiagramm.Kante
	 * @generated
	 */
	EClass getKante();

	/**
	 * Returns the meta object for the reference '{@link flowdiagramm.Kante#getVon <em>Von</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Von</em>'.
	 * @see flowdiagramm.Kante#getVon()
	 * @see #getKante()
	 * @generated
	 */
	EReference getKante_Von();

	/**
	 * Returns the meta object for the reference '{@link flowdiagramm.Kante#getNach <em>Nach</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Nach</em>'.
	 * @see flowdiagramm.Kante#getNach()
	 * @see #getKante()
	 * @generated
	 */
	EReference getKante_Nach();

	/**
	 * Returns the meta object for class '{@link flowdiagramm.Speicher <em>Speicher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Speicher</em>'.
	 * @see flowdiagramm.Speicher
	 * @generated
	 */
	EClass getSpeicher();

	/**
	 * Returns the meta object for the attribute '{@link flowdiagramm.Speicher#isIstGruppe <em>Ist Gruppe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ist Gruppe</em>'.
	 * @see flowdiagramm.Speicher#isIstGruppe()
	 * @see #getSpeicher()
	 * @generated
	 */
	EAttribute getSpeicher_IstGruppe();

	/**
	 * Returns the meta object for class '{@link flowdiagramm.Aktivitaet <em>Aktivitaet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aktivitaet</em>'.
	 * @see flowdiagramm.Aktivitaet
	 * @generated
	 */
	EClass getAktivitaet();

	/**
	 * Returns the meta object for the reference list '{@link flowdiagramm.Aktivitaet#getUnterstuetzendeFlows <em>Unterstuetzende Flows</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Unterstuetzende Flows</em>'.
	 * @see flowdiagramm.Aktivitaet#getUnterstuetzendeFlows()
	 * @see #getAktivitaet()
	 * @generated
	 */
	EReference getAktivitaet_UnterstuetzendeFlows();

	/**
	 * Returns the meta object for the reference list '{@link flowdiagramm.Aktivitaet#getKontrollierendeFlows <em>Kontrollierende Flows</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Kontrollierende Flows</em>'.
	 * @see flowdiagramm.Aktivitaet#getKontrollierendeFlows()
	 * @see #getAktivitaet()
	 * @generated
	 */
	EReference getAktivitaet_KontrollierendeFlows();

	/**
	 * Returns the meta object for the reference list '{@link flowdiagramm.Aktivitaet#getAnkommendeFlows <em>Ankommende Flows</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ankommende Flows</em>'.
	 * @see flowdiagramm.Aktivitaet#getAnkommendeFlows()
	 * @see #getAktivitaet()
	 * @generated
	 */
	EReference getAktivitaet_AnkommendeFlows();

	/**
	 * Returns the meta object for the reference list '{@link flowdiagramm.Aktivitaet#getAusgehendeFlows <em>Ausgehende Flows</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ausgehende Flows</em>'.
	 * @see flowdiagramm.Aktivitaet#getAusgehendeFlows()
	 * @see #getAktivitaet()
	 * @generated
	 */
	EReference getAktivitaet_AusgehendeFlows();

	/**
	 * Returns the meta object for class '{@link flowdiagramm.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person</em>'.
	 * @see flowdiagramm.Person
	 * @generated
	 */
	EClass getPerson();

	/**
	 * Returns the meta object for the attribute '{@link flowdiagramm.Person#isIstExperte <em>Ist Experte</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ist Experte</em>'.
	 * @see flowdiagramm.Person#isIstExperte()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_IstExperte();

	/**
	 * Returns the meta object for class '{@link flowdiagramm.Dokument <em>Dokument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dokument</em>'.
	 * @see flowdiagramm.Dokument
	 * @generated
	 */
	EClass getDokument();

	/**
	 * Returns the meta object for the attribute '{@link flowdiagramm.Dokument#isIstExperte <em>Ist Experte</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ist Experte</em>'.
	 * @see flowdiagramm.Dokument#isIstExperte()
	 * @see #getDokument()
	 * @generated
	 */
	EAttribute getDokument_IstExperte();

	/**
	 * Returns the meta object for class '{@link flowdiagramm.PersonDokument <em>Person Dokument</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person Dokument</em>'.
	 * @see flowdiagramm.PersonDokument
	 * @generated
	 */
	EClass getPersonDokument();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FlowdiagrammFactory getFlowdiagrammFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link flowdiagramm.impl.FlowDiagrammImpl <em>Flow Diagramm</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see flowdiagramm.impl.FlowDiagrammImpl
		 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getFlowDiagramm()
		 * @generated
		 */
		EClass FLOW_DIAGRAMM = eINSTANCE.getFlowDiagramm();

		/**
		 * The meta object literal for the '<em><b>Knoten</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FLOW_DIAGRAMM__KNOTEN = eINSTANCE.getFlowDiagramm_Knoten();

		/**
		 * The meta object literal for the '<em><b>Kanten</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FLOW_DIAGRAMM__KANTEN = eINSTANCE.getFlowDiagramm_Kanten();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOW_DIAGRAMM__NAME = eINSTANCE.getFlowDiagramm_Name();

		/**
		 * The meta object literal for the '{@link flowdiagramm.impl.ElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see flowdiagramm.impl.ElementImpl
		 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getElement()
		 * @generated
		 */
		EClass ELEMENT = eINSTANCE.getElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__NAME = eINSTANCE.getElement_Name();

		/**
		 * The meta object literal for the '{@link flowdiagramm.impl.KnotenImpl <em>Knoten</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see flowdiagramm.impl.KnotenImpl
		 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getKnoten()
		 * @generated
		 */
		EClass KNOTEN = eINSTANCE.getKnoten();

		/**
		 * The meta object literal for the '{@link flowdiagramm.impl.KanteImpl <em>Kante</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see flowdiagramm.impl.KanteImpl
		 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getKante()
		 * @generated
		 */
		EClass KANTE = eINSTANCE.getKante();

		/**
		 * The meta object literal for the '<em><b>Von</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference KANTE__VON = eINSTANCE.getKante_Von();

		/**
		 * The meta object literal for the '<em><b>Nach</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference KANTE__NACH = eINSTANCE.getKante_Nach();

		/**
		 * The meta object literal for the '{@link flowdiagramm.impl.SpeicherImpl <em>Speicher</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see flowdiagramm.impl.SpeicherImpl
		 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getSpeicher()
		 * @generated
		 */
		EClass SPEICHER = eINSTANCE.getSpeicher();

		/**
		 * The meta object literal for the '<em><b>Ist Gruppe</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPEICHER__IST_GRUPPE = eINSTANCE.getSpeicher_IstGruppe();

		/**
		 * The meta object literal for the '{@link flowdiagramm.impl.AktivitaetImpl <em>Aktivitaet</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see flowdiagramm.impl.AktivitaetImpl
		 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getAktivitaet()
		 * @generated
		 */
		EClass AKTIVITAET = eINSTANCE.getAktivitaet();

		/**
		 * The meta object literal for the '<em><b>Unterstuetzende Flows</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AKTIVITAET__UNTERSTUETZENDE_FLOWS = eINSTANCE.getAktivitaet_UnterstuetzendeFlows();

		/**
		 * The meta object literal for the '<em><b>Kontrollierende Flows</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AKTIVITAET__KONTROLLIERENDE_FLOWS = eINSTANCE.getAktivitaet_KontrollierendeFlows();

		/**
		 * The meta object literal for the '<em><b>Ankommende Flows</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AKTIVITAET__ANKOMMENDE_FLOWS = eINSTANCE.getAktivitaet_AnkommendeFlows();

		/**
		 * The meta object literal for the '<em><b>Ausgehende Flows</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AKTIVITAET__AUSGEHENDE_FLOWS = eINSTANCE.getAktivitaet_AusgehendeFlows();

		/**
		 * The meta object literal for the '{@link flowdiagramm.impl.PersonImpl <em>Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see flowdiagramm.impl.PersonImpl
		 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getPerson()
		 * @generated
		 */
		EClass PERSON = eINSTANCE.getPerson();

		/**
		 * The meta object literal for the '<em><b>Ist Experte</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__IST_EXPERTE = eINSTANCE.getPerson_IstExperte();

		/**
		 * The meta object literal for the '{@link flowdiagramm.impl.DokumentImpl <em>Dokument</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see flowdiagramm.impl.DokumentImpl
		 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getDokument()
		 * @generated
		 */
		EClass DOKUMENT = eINSTANCE.getDokument();

		/**
		 * The meta object literal for the '<em><b>Ist Experte</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOKUMENT__IST_EXPERTE = eINSTANCE.getDokument_IstExperte();

		/**
		 * The meta object literal for the '{@link flowdiagramm.impl.PersonDokumentImpl <em>Person Dokument</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see flowdiagramm.impl.PersonDokumentImpl
		 * @see flowdiagramm.impl.FlowdiagrammPackageImpl#getPersonDokument()
		 * @generated
		 */
		EClass PERSON_DOKUMENT = eINSTANCE.getPersonDokument();

	}

} //FlowdiagrammPackage
