/**
 */
package flowdiagramm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person Dokument</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see flowdiagramm.FlowdiagrammPackage#getPersonDokument()
 * @model
 * @generated
 */
public interface PersonDokument extends Speicher {
} // PersonDokument
