/**
 */
package flowdiagramm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link flowdiagramm.Person#isIstExperte <em>Ist Experte</em>}</li>
 * </ul>
 *
 * @see flowdiagramm.FlowdiagrammPackage#getPerson()
 * @model
 * @generated
 */
public interface Person extends Speicher {
	/**
	 * Returns the value of the '<em><b>Ist Experte</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ist Experte</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ist Experte</em>' attribute.
	 * @see #setIstExperte(boolean)
	 * @see flowdiagramm.FlowdiagrammPackage#getPerson_IstExperte()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isIstExperte();

	/**
	 * Sets the value of the '{@link flowdiagramm.Person#isIstExperte <em>Ist Experte</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ist Experte</em>' attribute.
	 * @see #isIstExperte()
	 * @generated
	 */
	void setIstExperte(boolean value);

} // Person
