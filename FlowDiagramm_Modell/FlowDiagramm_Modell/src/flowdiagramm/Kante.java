/**
 */
package flowdiagramm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Kante</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link flowdiagramm.Kante#getVon <em>Von</em>}</li>
 *   <li>{@link flowdiagramm.Kante#getNach <em>Nach</em>}</li>
 * </ul>
 *
 * @see flowdiagramm.FlowdiagrammPackage#getKante()
 * @model
 * @generated
 */
public interface Kante extends Element {
	/**
	 * Returns the value of the '<em><b>Von</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Von</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Von</em>' reference.
	 * @see #setVon(Knoten)
	 * @see flowdiagramm.FlowdiagrammPackage#getKante_Von()
	 * @model
	 * @generated
	 */
	Knoten getVon();

	/**
	 * Sets the value of the '{@link flowdiagramm.Kante#getVon <em>Von</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Von</em>' reference.
	 * @see #getVon()
	 * @generated
	 */
	void setVon(Knoten value);

	/**
	 * Returns the value of the '<em><b>Nach</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nach</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nach</em>' reference.
	 * @see #setNach(Knoten)
	 * @see flowdiagramm.FlowdiagrammPackage#getKante_Nach()
	 * @model
	 * @generated
	 */
	Knoten getNach();

	/**
	 * Sets the value of the '{@link flowdiagramm.Kante#getNach <em>Nach</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nach</em>' reference.
	 * @see #getNach()
	 * @generated
	 */
	void setNach(Knoten value);

} // Kante
