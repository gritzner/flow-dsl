/**
 */
package flowdiagramm.util;

import flowdiagramm.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see flowdiagramm.FlowdiagrammPackage
 * @generated
 */
public class FlowdiagrammAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FlowdiagrammPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlowdiagrammAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = FlowdiagrammPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FlowdiagrammSwitch<Adapter> modelSwitch =
		new FlowdiagrammSwitch<Adapter>() {
			@Override
			public Adapter caseFlowDiagramm(FlowDiagramm object) {
				return createFlowDiagrammAdapter();
			}
			@Override
			public Adapter caseElement(Element object) {
				return createElementAdapter();
			}
			@Override
			public Adapter caseKnoten(Knoten object) {
				return createKnotenAdapter();
			}
			@Override
			public Adapter caseKante(Kante object) {
				return createKanteAdapter();
			}
			@Override
			public Adapter caseSpeicher(Speicher object) {
				return createSpeicherAdapter();
			}
			@Override
			public Adapter caseAktivitaet(Aktivitaet object) {
				return createAktivitaetAdapter();
			}
			@Override
			public Adapter casePerson(Person object) {
				return createPersonAdapter();
			}
			@Override
			public Adapter caseDokument(Dokument object) {
				return createDokumentAdapter();
			}
			@Override
			public Adapter casePersonDokument(PersonDokument object) {
				return createPersonDokumentAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link flowdiagramm.FlowDiagramm <em>Flow Diagramm</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see flowdiagramm.FlowDiagramm
	 * @generated
	 */
	public Adapter createFlowDiagrammAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link flowdiagramm.Element <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see flowdiagramm.Element
	 * @generated
	 */
	public Adapter createElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link flowdiagramm.Knoten <em>Knoten</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see flowdiagramm.Knoten
	 * @generated
	 */
	public Adapter createKnotenAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link flowdiagramm.Kante <em>Kante</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see flowdiagramm.Kante
	 * @generated
	 */
	public Adapter createKanteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link flowdiagramm.Speicher <em>Speicher</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see flowdiagramm.Speicher
	 * @generated
	 */
	public Adapter createSpeicherAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link flowdiagramm.Aktivitaet <em>Aktivitaet</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see flowdiagramm.Aktivitaet
	 * @generated
	 */
	public Adapter createAktivitaetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link flowdiagramm.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see flowdiagramm.Person
	 * @generated
	 */
	public Adapter createPersonAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link flowdiagramm.Dokument <em>Dokument</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see flowdiagramm.Dokument
	 * @generated
	 */
	public Adapter createDokumentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link flowdiagramm.PersonDokument <em>Person Dokument</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see flowdiagramm.PersonDokument
	 * @generated
	 */
	public Adapter createPersonDokumentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //FlowdiagrammAdapterFactory
