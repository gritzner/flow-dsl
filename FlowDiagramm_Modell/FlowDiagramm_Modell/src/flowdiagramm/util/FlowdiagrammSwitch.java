/**
 */
package flowdiagramm.util;

import flowdiagramm.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see flowdiagramm.FlowdiagrammPackage
 * @generated
 */
public class FlowdiagrammSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FlowdiagrammPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlowdiagrammSwitch() {
		if (modelPackage == null) {
			modelPackage = FlowdiagrammPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case FlowdiagrammPackage.FLOW_DIAGRAMM: {
				FlowDiagramm flowDiagramm = (FlowDiagramm)theEObject;
				T result = caseFlowDiagramm(flowDiagramm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FlowdiagrammPackage.ELEMENT: {
				Element element = (Element)theEObject;
				T result = caseElement(element);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FlowdiagrammPackage.KNOTEN: {
				Knoten knoten = (Knoten)theEObject;
				T result = caseKnoten(knoten);
				if (result == null) result = caseElement(knoten);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FlowdiagrammPackage.KANTE: {
				Kante kante = (Kante)theEObject;
				T result = caseKante(kante);
				if (result == null) result = caseElement(kante);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FlowdiagrammPackage.SPEICHER: {
				Speicher speicher = (Speicher)theEObject;
				T result = caseSpeicher(speicher);
				if (result == null) result = caseKnoten(speicher);
				if (result == null) result = caseElement(speicher);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FlowdiagrammPackage.AKTIVITAET: {
				Aktivitaet aktivitaet = (Aktivitaet)theEObject;
				T result = caseAktivitaet(aktivitaet);
				if (result == null) result = caseKnoten(aktivitaet);
				if (result == null) result = caseElement(aktivitaet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FlowdiagrammPackage.PERSON: {
				Person person = (Person)theEObject;
				T result = casePerson(person);
				if (result == null) result = caseSpeicher(person);
				if (result == null) result = caseKnoten(person);
				if (result == null) result = caseElement(person);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FlowdiagrammPackage.DOKUMENT: {
				Dokument dokument = (Dokument)theEObject;
				T result = caseDokument(dokument);
				if (result == null) result = caseSpeicher(dokument);
				if (result == null) result = caseKnoten(dokument);
				if (result == null) result = caseElement(dokument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FlowdiagrammPackage.PERSON_DOKUMENT: {
				PersonDokument personDokument = (PersonDokument)theEObject;
				T result = casePersonDokument(personDokument);
				if (result == null) result = caseSpeicher(personDokument);
				if (result == null) result = caseKnoten(personDokument);
				if (result == null) result = caseElement(personDokument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Flow Diagramm</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Flow Diagramm</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFlowDiagramm(FlowDiagramm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElement(Element object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Knoten</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Knoten</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKnoten(Knoten object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Kante</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Kante</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKante(Kante object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Speicher</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Speicher</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSpeicher(Speicher object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aktivitaet</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aktivitaet</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAktivitaet(Aktivitaet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Person</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Person</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerson(Person object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dokument</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dokument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDokument(Dokument object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Person Dokument</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Person Dokument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonDokument(PersonDokument object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //FlowdiagrammSwitch
