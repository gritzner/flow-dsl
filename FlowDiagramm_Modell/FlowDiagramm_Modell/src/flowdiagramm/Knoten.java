/**
 */
package flowdiagramm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Knoten</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see flowdiagramm.FlowdiagrammPackage#getKnoten()
 * @model abstract="true"
 * @generated
 */
public interface Knoten extends Element {
} // Knoten
